/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from 'react-router-dom';
import {ApolloProvider} from '@apollo/client';
import {CookiesProvider} from 'react-cookie';
import {HelmetProvider} from 'react-helmet-async';
import {LoadingSplashScreen} from "./components/widgets/LoadingSplashScreen";
import {I18nService} from "./services/I18nService";
import {LiftyApplication} from './LiftyApplication';
import {getApolloClient} from "./utilities/getApolloClient";
import ErrorBoundary from './components/widgets/ErrorBoundary';
import {disableReactDevTools} from '@fvilers/disable-react-devtools';
import {SnackbarProvider, useSnackbar} from "notistack";
import ScrollToTop from './utilities/ScrollToTop';

const ApolloContainer = ({i18n, children}) => {
  const {enqueueSnackbar} = useSnackbar();
  return <ApolloProvider client={getApolloClient({i18n, enqueueSnackbar})}>{children}</ApolloProvider>;
};

let reactRootElement = document.getElementById('react-root');
I18nService().then(i18n => {
  ReactDOM.render(
    <React.StrictMode>
      <BrowserRouter>
        <HelmetProvider>
          <CookiesProvider>
            <SnackbarProvider maxSnack={4} anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
              <ApolloContainer i18n={i18n}>
                <React.Suspense fallback={<LoadingSplashScreen />}>
                  <ErrorBoundary>
                    <ScrollToTop>
                      <LiftyApplication />
                    </ScrollToTop>
                  </ErrorBoundary>
                </React.Suspense>
              </ApolloContainer>
            </SnackbarProvider>
          </CookiesProvider>
        </HelmetProvider>
      </BrowserRouter>
    </React.StrictMode>,
    reactRootElement
  )
});