

let ROUTES = {
  'INDEX': '/',
  "CONCEPT": "/concept",
  'SIGN_UP': '/signup',
  'PASSWORD_FORGOTTEN': "/password_forgotten",
  "SIGN_IN": "/signin",
  'DEMO': '/demo',
  'ADMIN': '/admin',

  'ADMIN_REFERENTIALS': '/admin/referentials',
  'ADMIN_REFERENTIAL': '/admin/referential/:id',

  'ADMIN_USERS': '/admin/users',
  'ADMIN_USER': '/admin/user/:id',
  'ADMIN_NEW_USER': '/admin/user',

  'ADMIN_PERSONS': '/admin/persons',
  'ADMIN_PERSON': '/admin/person/:id',
  'ADMIN_NEW_PERSON': '/admin/person',

  'ADMIN_JOBS': '/admin/jobs',
  'JOBS': '/jobs',
  'JOB': '/job/:id'
};

export {ROUTES};
