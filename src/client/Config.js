const Config = {
  colors: {
    dark: '#000000',
    blue: '#1C97C3', 
    darkBlue: '#2a3c46',
    white: 'white', 
    whiteGrey: "rgba(235, 235, 235, 0.2);",    
    whiteOpacity: 'rgba(255, 255, 255, 0.3)',
    grey: '#E6E6E6',
    greyOpacity: "rgba(168, 168, 168, 0.3)"
  },
  footer: {
    logoWidth: 350,
    height: 60
  },
  header: {
    height: 115,
    topPadding : 25,
    logoMaxHeight: 85,
  },
  logoTop: 20,
  theme: {breakpoint: 'sm'},
};
export default Config;
