import React, {Suspense} from 'react';
import {makeStyles, createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import {useQuery} from '@apollo/client';
import {Switch, Route, Redirect} from 'react-router-dom';
import Config from './Config';

import Header from './components/Header';
import Footer from './components/Footer';

import {LoadingSplashScreen} from "./components/widgets/LoadingSplashScreen";
import MaxWidthCentered from './components/widgets/MaxWidthCentered';

import Home from './components/Home';
import Concept from './components/Concept';

import AdminJobs from './components/admin/Jobs/Jobs';
import AdminReferentials from './components/admin/Referentials/Referentials';
import AdminReferential from './components/admin/Referentials/Referential';

//import AdminUsers from './components/admin/User/Users';
//import AdminUserEdit from './components/admin/User/UserEdit';
import AdminPersons from "./components/admin/Person/Persons";
import AdminPersonEdit from "./components/admin/Person/PersonEdit";


import Job from './components/Jobs/Job';
import Jobs from './components/Jobs/Jobs';
import SignIn from './components/Authentication/SignIn';
import SignUp from './components/Authentication/SignUp';
import PasswordForgotten from './components/Authentication/PasswordForgotten';
import Demo from './components/Demo';

import {ROUTES} from './routes';
import ImportedGraphics from './ImportedGraphics';
import {useResponsive} from './hooks/useResponsive';
import clsx from 'clsx';
import {EnvironmentContext} from './hooks/useEnvironment';
import {useLoggedUser} from "./hooks/useLoggedUser";
import {useLogout} from "./hooks/useLogout";
import {gqlEnvironmentQuery} from './hooks/useEnvironment';

const theme = createMuiTheme({
  typography: {
    fontFamily: ['Lato', 'Segoe UI Regular', 'Segoe UI', 'Roboto', '"Helvetica Neue"', 'Arial', 'sans-serif'].join(','),
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 800,
      lg: 1180,
      xl: 1600,
    },
  },
});

const useStyles = makeStyles((theme) => ({
  indexRoot: {
    overflow: 'hidden',
    fontSize: 'calc(8px + 2vmin)',
    height: '100%',
    backgroundColor: Config.colors.whiteGrey,
    color: Config.colors.dark,
    display: 'flex',
    flexDirection: 'column',
    paddingTop: `${Config.header.topPadding}px`
  },
  indexDesktop: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'row'
  },
  indexMobile: {
    display: 'flex',
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexDirection: 'column'
  },
  fullMaxHeight: {
    minHeight: `calc(100vh - ${Config.header.topPadding}px - ${Config.footer.height}px)`,
  },
  indexContent: {
    flex: 1,
    margin: 'auto',
    marginTop: Config.header.height,
    padding: Config.logoTop,
    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      padding: '10px',
    },
    paddingTop: '0px',
    display: 'flex',
    // flex: '1 0 auto' /* Prevent Chrome, Opera, and Safari from letting these items shrink to smaller than min size. */,
    zIndex: 99,
    // backgroundColor: Config.colors.whiteOpacity
  },
  relativeParent: {
    position: 'relative'
  },

  absoluteBackground: {
    position: 'fixed',
    background: `linear-gradient(0.25turn, ${Config.colors.greyOpacity}, ${Config.colors.whiteOpacity}, ${Config.colors.whiteOpacity}, ${Config.colors.whiteOpacity}, ${Config.colors.greyOpacity}), url(${ImportedGraphics.home_back_small})`,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    height: '100%',
    width: "100%",
    left: "0px",
    top: "0px",
  },

}));



export function LiftyApplication(props) {
  const classes = useStyles();
  const {isDesktop} = useResponsive();
  const {data: envData, loading: envLoading} = useQuery(gqlEnvironmentQuery);
  const {isLogged, isAdmin, loading, user} = useLoggedUser();   
  const logout = useLogout("/");

  return (
    <MuiThemeProvider theme={theme}>
      <EnvironmentContext.Provider value={envData?.environment}>
        <CssBaseline />
        <div className={clsx(classes.indexRoot, classes.relativeParent)}>
          <div className={clsx(classes.fullMaxHeight, isDesktop ? classes.indexDesktop : classes.indexMobile)}>
            <Header isDesktop={isDesktop} isLogged={isLogged} isAdmin={isAdmin} logout={logout} />

            <div className={classes.indexContent}>
              <Choose>
                <When condition={loading || envLoading}>
                  <LoadingSplashScreen />
                </When>

                <When condition={isAdmin}>
                  <MaxWidthCentered size={'xl'}>
                    <Switch>
                      {/* 
                        <Route exact path={ROUTES.ADMIN_USERS} component={AdminUsers} />
                        <Route exact path={ROUTES.ADMIN_USER} component={AdminUserEdit} />
                        <Route exact path={ROUTES.ADMIN_NEW_USER} component={AdminUserEdit} />
                      */}
                      <Route exact path={ROUTES.ADMIN_PERSONS} component={AdminPersons} />
                      <Route exact path={ROUTES.ADMIN_PERSON} component={AdminPersonEdit} />
                      <Route exact path={ROUTES.ADMIN_NEW_PERSON} component={AdminPersonEdit} />
                      <Route exact path={ROUTES.ADMIN_JOBS} component={AdminJobs} />
                      <Route exact path={ROUTES.ADMIN_REFERENTIALS} component={AdminReferentials} />
                      <Route exact path={ROUTES.ADMIN_REFERENTIAL} component={AdminReferential} />
                      <Route exact path={ROUTES.SIGN_UP} component={SignUp} />
                      <Route exact path={ROUTES.JOB} component={Job} />
                      <Redirect to={ROUTES.ADMIN_JOBS} />
                    </Switch>
                  </MaxWidthCentered>
                </When>

                <When condition={isLogged}>
                  <MaxWidthCentered size={'lg'}>
                    <Switch>
                      <Route exact path={ROUTES.JOB} component={Job} />
                      <Route exact path={ROUTES.JOBS} render={() => <Jobs personId={user?.id} />} />
                      <Route exact path={ROUTES.CONCEPT} component={Concept} />
                      <Route exact path={ROUTES.DEMO} component={Demo} />
                      <Route exact path={ROUTES.PASSWORD_FORGOTTEN} component={PasswordForgotten} />
                      <Route exact path={ROUTES.SIGN_UP} component={SignUp} />
                      <Redirect to={ROUTES.JOBS} />
                    </Switch>
                  </MaxWidthCentered>
                </When>

                <Otherwise>
                  <MaxWidthCentered size={'lg'}>
                    <Switch>
                      <Route exact path={ROUTES.CONCEPT} component={Concept} />
                      <Route exact path={ROUTES.DEMO} component={Demo} />
                      <Route exact path={ROUTES.PASSWORD_FORGOTTEN} component={PasswordForgotten} />
                      <Route exact path={ROUTES.SIGN_UP} component={SignUp} />
                      <Route exact path={ROUTES.SIGN_IN} component={SignIn} />
                      <Route exact path={ROUTES.INDEX} component={Home} />
                      <Redirect to={ROUTES.INDEX} />
                    </Switch>
                  </MaxWidthCentered>
                </Otherwise>
              </Choose>
            </div>

          </div>
          <Footer test={user?.userAccount?.username} />
          {!isAdmin &&
            <div className={classes.absoluteBackground} />
          }
        </div>
      </EnvironmentContext.Provider>
    </MuiThemeProvider>
  );
}