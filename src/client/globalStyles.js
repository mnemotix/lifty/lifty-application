import Config from './Config';
import {makeStyles} from '@material-ui/core/styles';

const globalStyles = makeStyles((theme) => ({

  tester: {
    border: '1px solid green',
    padding: '1px',
  },
  tester2: {
    border: '2px dotted red',
    padding: '1px',
  },
  tester3: {
    backgroundColor: '#F4A75F',
  },
  flexCenter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    //flex: 1,
  },
  flexSpaceAround: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    flex: 1,
    width: '100%',
  },   
  flexSpaceBetween: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    width: '100%',
  },
  flexStart: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',

  },
  flexEnd: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flex: 1,
  },
  flexDirectionCol: {
    flexDirection: 'column',
  },
  p20: {
    padding: '20px !important',
  },
  p10: {
    padding: '10px !important',
  },
  fullHeight: {
    height: "85vh"
  },
  alignCenter: {
    textAlign: "center"
  },
  noPaddingLR: {
    paddingRight: '0px !important',
    paddingLeft: '0px !important',
    [theme.breakpoints.between('sm', 'md')]: {
      maxWidth: '90vw !important'
    },
  },

}));

export default globalStyles;