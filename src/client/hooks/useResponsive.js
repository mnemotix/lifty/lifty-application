import {useTheme} from "@material-ui/core/styles";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import Config from '../Config';

export function useResponsive() {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("xs"));
  const isTablet = useMediaQuery(theme.breakpoints.between("xs", Config.theme.breakpoint));

  /**
   * for now isDesktop is true even is isTablet also true 
   * separating both will need a lot of work
   * isTablet is just used in one component for the moment
   */
  return {isMobile, isTablet, isDesktop: !isMobile};
}
