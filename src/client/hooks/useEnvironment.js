import {createContext, useContext} from 'react';

import {gql} from '@apollo/client';
import EnvVars from '../../server/config/environment';

export const EnvironmentContext = createContext();

export const gqlEnvironmentQuery = gql`
  query EnvironmentQuery {
    environment {
      ${Object.entries(EnvVars)
    .filter(([variable, {exposeInGraphQL}]) => exposeInGraphQL === true)
    .map(([variable]) => variable)}
    }
  }
`;

/**
 * @param {string} environmentName
 * @param isBoolean
 * @param isNumber
 */
export function useEnvironment(environmentName, {isBoolean, isFloat, isInt} = {}) {
  const environments = useContext(EnvironmentContext);
  let environment = environments?.[environmentName];

  if (isBoolean) {
    environment = environment && !!parseInt(environment);
  }

  if (environment) {
    if (isFloat) {
      environment = parseFloat(environment);
    }

    if (isInt) {
      environment = parseInt(environment);
    }

    return environment;
  }
}
