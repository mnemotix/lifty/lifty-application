import home_back_small from '../../public/images/home_back_small.png';
import logo from '../../public/images/logo.svg';
import logoBig from '../../public/images/logo-headerBig.svg';
import copyright from '../../public/images/copyright.svg';
import copyright2021 from '../../public/images/copyright2021.svg';
import copyright2022 from '../../public/images/copyright2022.svg';
import copyright2023 from '../../public/images/copyright2023.svg';
import copyright2024 from '../../public/images/copyright2024.svg';
import copyright2025 from '../../public/images/copyright2025.svg';
import copyright2026 from '../../public/images/copyright2026.svg';

const ImportedGraphics = {
  home_back_small,
  logo,
  logoBig,
  copyright,
  copyright2021,
  copyright2022,
  copyright2023,
  copyright2024,
  copyright2025,
  copyright2026,
}

export default ImportedGraphics;