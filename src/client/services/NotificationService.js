import i18next from 'i18next';
import {NotificationService} from '@mnemotix/synaptix-client-toolkit';

export const notificationService = new NotificationService({
  yesDefaultText: i18next.init().then(t => t('CONFIRMATION_MODAL.YES')),
  noDefaultText: i18next.init().then(t => t('CONFIRMATION_MODAL.NO'))
});

