/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import React from 'react';
import { forwardRef } from "react";
import {Link as RouterLink} from "react-router-dom";
import Link from "@material-ui/core/Link";

export function createLink({to, text, children, ...props} = {}) {
  if(!to){
    console.log("warning, fix this createLink ( maybe route used as 'to' is undefined or null ) ",{to , text})
  }
  // The use of React.forwardRef will no longer be required for react-router-dom v6.
  // See https://github.com/ReactTraining/react-router/issues/6056
  const WrappedLink = forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);

  return (
    <Link component={WrappedLink} to={to} {...props}>
      {text || children || ""}
    </Link>
  );
}
