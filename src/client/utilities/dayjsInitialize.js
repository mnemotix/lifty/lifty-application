import dayjs from "dayjs";
import 'dayjs/locale/fr';
import LocalizedFormat from 'dayjs/plugin/localizedFormat';

export function dayjsInitialize() {
  dayjs.extend(LocalizedFormat);
  dayjs.locale('fr');
}
