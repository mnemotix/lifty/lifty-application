import React from 'react';
import {useTranslation} from "react-i18next";
import {Button} from "@material-ui/core";
import Alert from '@material-ui/lab/Alert';
import {makeStyles} from "@material-ui/core/styles";
import {alertMapping} from "./tools";


const useStyles = makeStyles((theme) => ({
  jobButtonsForFilterRoot: {
    width: "100%",
    display: 'flex',
    flexWrap:'wrap',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  button: {
    minWidth: '100px'
  },
  buttonContent: {
    minHeight: '50px',
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
}));


export function JobButtonsForFilter({setFilters}) {
  const {t} = useTranslation();
  const classes = useStyles();
  return (
    <div className={classes.jobButtonsForFilterRoot}>
      <Button className={classes.button} onClick={() => setFilters(null)}>
        <span className={classes.buttonContent}>
          {t('REMOTE_TABLE.FILTER.ALL')}
        </span>
      </Button>

      {Object.entries(alertMapping).map(([key, value]) => {
        const {text, severity} = value;
        return (
          <Button className={classes.button} onClick={() => setFilters(key)} key={key}>
            <Alert severity={severity} className={classes.buttonContent}>
              {t(text)}
            </Alert>
          </Button>
        );

      })}
    </div>
  );
};

