import React, {useEffect, useState} from "react";

import {useParams} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import {CircularProgress, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper} from "@material-ui/core";

import {useLazyQuery} from "@apollo/client";
import dayjs from "dayjs";
import {LoadingSplashScreen} from "../widgets/LoadingSplashScreen";
import Config from "../../Config";
import {gqlJob} from "../admin/Jobs/gql/Job.gql";
import {gqlJobStatus} from "../admin/Jobs/gql/JobStatus.gql";
import {JobStatus} from "../widgets/JobStatus.js";
import {SetupJob} from "./SetupJob.js";

const useStyles = makeStyles((theme) => ({
  jobRoot: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    width: "100%",
  },
  breadcrumbs: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  content: {
    marginTop: "25px",
    fontSize: '16px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    flex: 1,
    width: "100%",
    minHeight: "50vh"
  },
  textcenter: {
    textAlign: 'center'
  },
  item: {
    padding: '15px'
  },
  link: {
    fontSize: '20px',
    color: Config.colors.darBlue,
    padding: "10px 0px",
    textDecoration: 'underline',
    fontStyle: 'italic'
  }
}));

const _timeInterval = 1000 * 60; // toutes les minutes

export default function Job() {
  const classes = useStyles();
  const {t} = useTranslation();

  let id = false;
  const params = useParams();
  if (params.id) {
    id = decodeURIComponent(params.id);
  }

  const [recognizer, setRecognizer] = useState(null);
  const [getRecognizer, {loading}] = useLazyQuery(gqlJob, {
    variables: {id},
    fetchPolicy: 'cache-and-network',
    onCompleted: ({recognizer}) => {
      setRecognizer(recognizer)
    }
  });

  // to refresh only status 
  const [getStatus, {loading: loadingStatus}] = useLazyQuery(gqlJobStatus, {
    variables: {id},
    fetchPolicy: 'no-cache',
    onCompleted: (data) => {
      setRecognizer({...recognizer, status: data.recognizer.status});
    }
  });

  useEffect(() => {
    if (id) {
      getRecognizer();
    }
  }, [id]);

  let intervl;
  useEffect(() => {
    if (recognizer?.status) {
      // si erreur ou succes on arrete 
      if (!!recognizer.status.done || (!!recognizer.status.error && recognizer.status.error.length > 0)) {
        if (intervl) {
          clearInterval(intervl);
        }
      } else if (!recognizer.status.unused) {
        intervl = setInterval(getStatus, _timeInterval);
      }
    }
    return () => { // cleanup
      if (intervl) {
        clearInterval(intervl);
      }
    };
  }, [recognizer]);

  if (id && loading) {
    return <LoadingSplashScreen />;
  }

  function renderTableRow(title, component, loading) {
    return (<TableRow key={title}>
      <TableCell component="th" scope="row">
        {title}{":"}
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </TableCell>
      <TableCell align="left"> {component}</TableCell>
    </TableRow>)
  }

  return (
    <div className={classes.jobRoot}>
      <div className={classes.textcenter}>Merci de conserver le lien de cette page pour suivre l'état d'avancement de votre tâche
        <div className={classes.link}>{window.location.href}</div>
      </div>


      <div className={classes.content}>
      <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableBody>
              {renderTableRow(t("COMMON.CREATED_AT"), recognizer?.createdAt ? dayjs(recognizer.createdAt).format("L LT") : '')}
              {renderTableRow(t("PERSON.EMAIL"), recognizer?.providedby?.mainEmail?.email || '')}
              {renderTableRow(t("JOB.INDEXNAME"), recognizer?.indexName || '')}
              {renderTableRow(t("JOB.CANUSEPREMIUMVOC"), recognizer?.canUsePremium ? 'Oui' : 'Non')}
              {!recognizer?.status?.unused &&
                renderTableRow(
                  <span onClick={getStatus}> {t("JOB.STATUS")}</span>,
                  <JobStatus status={recognizer?.status} id={id} />,
                  loadingStatus
                )
              }
            </TableBody>
          </Table>
        </TableContainer>

        {recognizer?.status?.unused &&
          <SetupJob id={id} canUsePremium={recognizer?.canUsePremium} onComplete={getRecognizer} />
        }
      </div>

    </div >
  );
}
