import React, {useState} from 'react';
import clsx from 'clsx';
import {useMutation, gql} from "@apollo/client";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import {Typography, Paper, Button, Stepper, Step, StepLabel, StepButton, StepContent} from "@material-ui/core";
import BackupIcon from '@material-ui/icons/Backup';
import {useSnackbar} from "notistack";
import CSVFileValidator from 'csv-file-validator'

import {LoadingButton} from "../widgets/Button/LoadingButton";
import {GreenButton} from "../widgets/Button/GreenButton";
import {ReferentialChooser} from "../widgets/ReferentialChooser/ReferentialChooser";
import {ExplainCSV} from "../widgets/CSV/ExplainCSV";
import {CSVConfig} from "../widgets/CSV/tools.js"
import {PreCode} from "../widgets/PreCode.js"


// enable or not the csv validator, another one is still present in backend in JobController
const enableCsvValidator = true;

const gqlUpdateJobWithUpload = gql`
  mutation UpdateRecognizer($input: UpdateRecognizerInput!) {
    updateRecognizer(input: $input) {
      updatedObject {
        id				
        indexName      
        status {
          unused
          pending
        }
        dirPath
        filePath
      }
    }
  }
`;


const useStyles = makeStyles((theme) => ({
  actionsRoot: {
    width: '100%',
  },
  stepContent: {
    paddingTop: "15px",
    paddingBottom: "15px",
  },
  actionsContainer: {
    marginBottom: theme.spacing(2),
  },
  grey: {
    color: "grey",
    fontStyle: 'italic'
  },
  smallFont: {
    fontSize: "calc(2px + 2vmin)"
  },
  leftPadding15: {
    paddingLeft: "15px"
  }
}));

/**
 * Setup a job (how have status.unused === true ) by adding a vocabulary indexname, a csv path and sending an amqp message 
 * 
 * 
 * @param {string} id
 * @param {boolean} canUsePremium : can current user use premium vocabulary
 * @param {function} onComplete : callback to refresh parent onComplete
 * @returns 
 */
export function SetupJob({id, canUsePremium = false, onComplete = null}) {
  const classes = useStyles();
  const {enqueueSnackbar} = useSnackbar();
  const {t} = useTranslation();

  // contain the choosen index
  const [selectedIndex, setSelectedIndex] = useState({});
  // contain file 
  const [file, setFile] = useState(null);
  // upload file only once , become true after csv is uploaded successfully
  const [disableStart, setDisableStart] = useState(false);

  const [activeStep, setActiveStep] = useState(0);
  const handleNext = () => {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };
  const handleStep = (step) => {
    setActiveStep(step);
  };


  // check a CSV file in frontend
  const [csvErrors, setCsvErrors] = useState(false);
  function onChangeFile({target: {validity, files: [file]}}) {
    file && validity.valid && setFile(file);
    if (enableCsvValidator) {
      try {
        CSVFileValidator(file, CSVConfig)
          .then(csvData => {
            if (csvData?.inValidMessages?.length > 0) {
              setCsvErrors(csvData.inValidMessages);
            } else {
              setCsvErrors(false);
            }
          })
      } catch (error) {
        setCsvErrors(false);
        console.log(error)
      }
    }
  }

  const [mutate, {loading, error}] = useMutation(gqlUpdateJobWithUpload, {
    fetchPolicy: 'no-cache',
    onCompleted: data => {
      enqueueSnackbar(t("JOB.CSV_UPLOADED"), {variant: "success"});
      setDisableStart(true);
      onComplete && onComplete();
    }
  });


  function handleStart() {
    mutate({
      variables: {
        "input": {
          "objectId": id,
          "file": file,
          "schemeId": selectedIndex.indexName,
          "objectInput": {
            "indexName": selectedIndex.indexName,
          }
        }
      }
    });
  };

  let csvValid = enableCsvValidator ? file && !csvErrors : file;

  return (
    <div className={classes.actionsRoot}>

      <Stepper activeStep={activeStep} orientation="vertical">
        <Step key={0}>
          <StepButton onClick={() => handleStep(0)} completed={!!selectedIndex?.indexName}>
            {t("REFERENTIAL.CHOOSE_ONE_SCHEME")}{selectedIndex?.label && <>{": "}<span className={classes.grey}>{selectedIndex?.label}</span></>}
          </StepButton>
          <StepContent>
            <div className={classes.stepContent}>
              <ReferentialChooser
                selectedIndex={selectedIndex}
                setSelectedIndex={setSelectedIndex}
                userCanUsePremium={canUsePremium}
              />
            </div>
            <div className={classes.actionsContainer}>
              <div>
                <GreenButton
                  variant="contained"
                  color="primary"
                  disabled={!selectedIndex?.indexName}
                  onClick={handleNext}
                  className={classes.button}
                >
                  {t("ACTIONS.NEXT")}
                </GreenButton>
              </div>
            </div>
          </StepContent>
        </Step>
        <Step key={1}>
          <StepButton onClick={() => handleStep(1)} completed={!!file}>
            {t("JOB.ADD_YOUR_CSV")}{file && <>{": "}<span className={classes.grey}>{file?.name}</span></>}
          </StepButton>
          <StepContent>
            <div className={classes.stepContent}>
              <ExplainCSV />
              {/*<input type="file" id="file" name="file" required
                onChange={onChangeFile} accept=".csv,text/csv" />*/}

              <input
                required
                accept=".csv,text/csv"
                className={classes.input}
                style={{display: 'none'}}
                id="raised-button-file"
                onChange={onChangeFile}
                type="file"
              />
              <label htmlFor="raised-button-file">
                <Button variant="contained" color="primary" component="span"
                  className={classes.button}
                  startIcon={<BackupIcon />}>
                  {t("ACTIONS.UPLOAD_CSV")}
                </Button>
              </label>
              {file && <span className={clsx(classes.grey, classes.smallFont, classes.leftPadding15)}>{'fichier ajouté: ' + file?.name}</span>}

              {csvErrors &&
                <PreCode message={csvErrors.join('\n')} error={true} />
              }

            </div>
            <div className={classes.actionsContainer}>
              <div>
                <GreenButton
                  variant="contained"
                  color="primary"
                  disabled={!csvValid}
                  onClick={handleNext}
                  className={classes.button}
                >
                  {t("ACTIONS.NEXT")}
                </GreenButton>
              </div>
            </div>
          </StepContent>
        </Step>
        <Step key={2}>
          <StepLabel>{t("JOB.START")}</StepLabel>
          <StepContent>
            <div className={classes.stepContent}>
              {t("JOB.ANALYSE_FILE") + " "}<span className={classes.grey}>{file?.name}</span>{" " + t("JOB.ON_SCHEME") + " "}<span className={classes.grey}>{selectedIndex?.label}</span>.
            </div>
            <div className={classes.actionsContainer}>
              <div>
                <LoadingButton
                  variant="contained"
                  color="primary"
                  loading={loading}
                  disabled={disableStart}
                  useGreenButton={true}
                  onClick={handleStart} >
                  {t("JOB.START")}
                </LoadingButton>

              </div>
            </div>
          </StepContent>
        </Step>
      </Stepper>

      {error && (
        <Paper square elevation={0} className={classes.error}>
          <Typography>{t("JOB.ERROR_HAPPENED")}</Typography>
        </Paper>
      )}
    </div >
  );
}


