import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import {Typography, Breadcrumbs} from "@material-ui/core";
import {PersonJobs} from "../admin/Jobs/PersonJobs";

const useStyles = makeStyles((theme) => ({
  jobsRoot: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    width:'100%'
  },
  breadcrumbs: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  }
}));


export default function Jobs({personId}) {
  const {t} = useTranslation();
  const classes = useStyles();

  if (!personId) {
    return null
  }
  return <div className={classes.jobsRoot}>
    <Breadcrumbs className={classes.breadcrumbs}>
      <Typography variant="h4" gutterBottom>
        {t("JOB.MY_LIST") + " :"}
      </Typography>
    </Breadcrumbs>

    <PersonJobs id={personId} isAdmin={false} />
  </div>
}