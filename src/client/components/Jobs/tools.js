export const alertMapping = {
  unused: {severity: "warning", text: "JOB.UNUSED"},
  pending: {severity: "warning", text: "JOB.PENDING"},
  processing: {severity: "info", text: "JOB.PROCESSING"},
  done: {severity: "success", text: "JOB.DONE"},
  error: {severity: "error", text: "JOB.ERROR"}
}

export const errorMapping = {
  "liftyconnectexception": "Connexion impossible",
  "liftyextractexception": "Le fichier CSV est invalide",
  "liftyrecognizeexception": "Recognize Exception",
  "liftyannotationexception": "Annotation Exception",
  "liftyoutputexception": "Output Exception",
}



/**
 * Gql to filter jobs by status
 * @param {*} filter 
 * @returns 
 */
export function getGqlFilter(filter) {
  switch (filter) {
    case 'unused':
      return ["statusUnused:true"];
    case 'pending':
      return ["statusPending:true"];
    case 'processing':
      return ["statusProcessing:true"];
    case 'done':
      return ["statusDone:true"];
    case 'error':
      return ["statusError:*"];
    case null:
    default:
      return [];
  }
}