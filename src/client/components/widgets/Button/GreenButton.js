import React from 'react';
import {withStyles, makeStyles} from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import {green} from '@material-ui/core/colors';



const ColorButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText(green[500]),
    backgroundColor: green[500],
    '&:hover': {
      backgroundColor: green[700],
    },
  },
}))(Button);

const useStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1),
  },
}));


export  function GreenButton({children, ...props}) {
  const classes = useStyles();

  return (
    <ColorButton variant="contained" color="primary" className={classes.margin} {...props} >
      {children}
    </ColorButton>
  );
}
