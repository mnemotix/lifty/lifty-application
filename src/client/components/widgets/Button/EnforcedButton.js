import React from 'react';
import {Tooltip} from "@material-ui/core";
import {LoadingButton} from "../../widgets/Button/LoadingButton";
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import {Link} from "react-router-dom";


const useStyles = makeStyles((theme) => ({
  tooltip: {
    textAlign: "center",
  },
}));

/**
 * Display a button that is disabled if locked property set to true
 * then display a tooltip.
 */
export const EnforcedButton = ({locked, lockedMessage, children, ...props} = {}) => {
  const {t} = useTranslation();
  const classes = useStyles();

  return locked ? (
    <Tooltip title={lockedMessage || t("ACTIONS.NOT_ALLOWED")} arrow classes={{tooltip: classes.tooltip}}>
      <span>
        <LoadingButton variant="contained"
          color="primary"
          component={Link} {...props} disabled={true}>
          {children}
        </LoadingButton>
      </span>
    </Tooltip>
  ) : (
    <LoadingButton variant="contained"
      color="primary"
      component={Link} {...props}>
      {children}
    </LoadingButton>
  );
};
