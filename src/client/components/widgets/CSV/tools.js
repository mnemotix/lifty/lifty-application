

/*
 centralised config for CSV
 tableRows are displayed in the table 
 CSVConfig are used for csv validator
 */



const requiredError = (headerName, rowNumber, columnNumber) => {
  return `${headerName} is required in the row number : ${rowNumber}, column number : ${columnNumber} `
}
const validateError = (headerName, rowNumber, columnNumber) => {
  return `${headerName} is not valid in the row number : ${rowNumber}, column number : ${columnNumber} `
}
const uniqueError = (headerName, rowNumber) => {
  return `${headerName} is not unique at the row number : ${rowNumber} `
}

export const CSVConfig = {
  headers: [
    {name: 'id', inputName: 'id', required: true, requiredError, unique: true, uniqueError},
    {name: 'content', inputName: 'content', required: true, requiredError},
    {name: 'nameSpace', inputName: 'nameSpace', required: false},
    {name: 'nodeClass', inputName: 'nodeClass', required: false},
  ]
}

export const tableRows = [{
  name: "id",
  optionnal: "Non",
  type: "Chaine de caractère sans espace servant d'identifiant unique",
  exemple: `"www.agent_de_fabrication.com"`
}, {
  name: "content",
  optionnal: "Non",
  type: "Chaine de caractère",
  exemple: `"Description du poste : \n L'agence Jobtruck, Agence pour l'emploi CDI, CDD, Intérim  recrute un agent de fabrication (h/f) sur Arles et ses environs . \n Votre mission consistera ? : \n - Préparer et trier les produits \n - Conditionner et emballer les produits \n - Nettoyer la chaîne de production \n - Suivre les règles de sécurité \n \n Type d'emploi : Temps plein \n Type de contrat : Intérim \n Dur?e mission : 2 semaines \n Taux horaire : 10.15 euros \n \n Permis B"`
}, {
  name: "nameSpace",
  optionnal: "Oui",
  type: "Url",
  exemple: `"http://ontology.datasud.fr/openemploi/"`
}, {
  name: "nodeClass",
  optionnal: "Oui",
  type: "Nom de la classe",
  exemple: `"Offer"`
}];
