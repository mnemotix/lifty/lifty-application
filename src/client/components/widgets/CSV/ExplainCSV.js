
import React from 'react';

import {makeStyles} from "@material-ui/core/styles";
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow} from "@material-ui/core";
import {tableRows} from "./tools.js"


const useStyles = makeStyles((theme) => ({
  grey: {
    color: "grey",
    fontStyle: 'italic',
    // fontSize: 'calc(4px + 2vmin)'
  },
  ExplainCSVRoot: {
    paddingBottom: "15px",
  },
  table: {
    minWidth: 650,
    margin: "20px 0px",
  },
}));


export function ExplainCSV() {
  const classes = useStyles();



  return (<div className={classes.ExplainCSVRoot}>
    Voici les colonnes autorisées dans le CSV : <br />

    <Table className={classes.table} aria-label="simple table">
      <TableHead>
        <TableRow>
          <TableCell>Nom de la colonne</TableCell>
          <TableCell align="left">Optionnel</TableCell>
          <TableCell align="left">Type</TableCell>
          <TableCell align="left">Exemple</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
        {tableRows.map((row, index) => (
          <TableRow key={row.name + index}>
            <TableCell component="th" scope="row">
              {row.name}
            </TableCell>
            <TableCell align="left">{row.optionnal}</TableCell>
            <TableCell align="left">{row.type}</TableCell>
            <TableCell align="left">{row.exemple}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>

    <div className={classes.grey}>Utiliser la virgule `,` comme séparateur et l'encodage en UTF-8.</div>
    <div className={classes.grey}>Le nom des colonnes `nameSpace` et `nodeClass` est sensible à la casse.</div>

    ??? Rajout d'un Fichier exemple :  example.CSV ???
  </div>)
}