import React, {useState} from 'react';
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import {useMutation} from "@apollo/client";
import {useSnackbar} from "notistack";
import {Tooltip, Chip} from '@material-ui/core';
import {gqlReferentials} from "./gql/Referentials.gql";
import {gqlUpdateReferential} from "./gql/Referentials.gql";
import {CollectionView} from "../../widgets/CollectionView/CollectionView";
import {LoadingButton} from "../../widgets/Button/LoadingButton";

// get the last item of a path
function getLastItem(thePath) {
  return thePath.substring(thePath.lastIndexOf('/') + 1)
}

function createIndexName(vocabularyUri, schemeUri) {
  return `${vocabularyUri.slice(vocabularyUri.lastIndexOf("/") + 1)}-${schemeUri.slice(schemeUri.lastIndexOf("/") + 1)}`
}

const useStyles = makeStyles((theme) => ({
  chipContainer: {
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    listStyle: 'none',
    padding: theme.spacing(0.5),
    margin: 0,
  },
  chip: {
    margin: theme.spacing(0.5),
  },
}));


/**
 * 
 * @param {bool} userCanUsePremium : userCanUsePremium is used when isAdmin is false, it prevent user to select premium vocabulary when he is not allowed
 */
function getColumns({isAdmin, selectedIndex, setSelectedIndex, classes, t, userCanUsePremium}) {
  let columns = [
    {
      name: "altLabel",
      label: t("REFERENTIAL.ALTLABEL"),
      options: {filter: true, sort: true}
    },
    {
      name: "prefLabel",
      label: t("REFERENTIAL.PREFLABEL"),
      options: {filter: true, sort: true}
    },
    {
      name: "schemes",
      label: setSelectedIndex ? t("REFERENTIAL.CHOOSE_ONE_SCHEME") : t("REFERENTIAL.SCHEMES"),
      options: {
        customBodyRender: (schemes, {row}) => {
          if (schemes?.edges) {
            return <div className={classes.chipContainer}>
              {schemes?.edges?.map(({node}) => {
                const label = node?.prefLabel || getLastItem(node?.uri);
                const indexName = createIndexName(row?.uri, node?.uri);

                if (row?.premium && !userCanUsePremium) {
                  return <Tooltip title={t("REFERENTIAL.PREMIUM_ONLY")} key={"tooltip" + node?.uri}>
                    <Chip label={label} key={node?.uri} color={indexName === selectedIndex?.indexName ? "primary" : 'default'} className={classes.chip}
                      clickable={false}
                      onClick={() => null} />
                  </Tooltip>
                }
                return <Chip label={label} key={node?.uri} color={indexName === selectedIndex?.indexName ? "primary" : 'default'} className={classes.chip}
                  clickable={!!setSelectedIndex && (row?.premium && userCanUsePremium)}
                  onClick={() => setSelectedIndex && setSelectedIndex({indexName, label})}
                />
              })}
            </div>
          } else {
            return null
          }
        }
      }
    }, {
      name: "premium",
      label: t("REFERENTIAL.PREMIUM"),
      options: {
        filter: true, sort: true,
        customBodyRender: (premium, {row}) => {
          return <ButtonPremium id={row.uri} isPremium={premium} isAdmin={isAdmin} />;
        }
      }
    }
  ]
  return columns;
}


/**
 * affiche un bouton permettant de changer la valeur du premium pour ce vocabulary
 * @param {*} param0 
 * @returns 
 */
function ButtonPremium({id, isPremium, isAdmin}) {
  const [premium, setPremium] = useState(!!isPremium);

  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();
  const [mutateReferential, {loading: savingRef}] = useMutation(gqlUpdateReferential, {
    //fetchPolicy: 'no-cache',
    onCompleted: data => {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
      setPremium(data?.updateVocabulary?.updatedObject?.premium)
    }
  });

  function switchPremium() {
    mutateReferential({
      variables: {
        "input": {
          "objectId": id,
          "objectInput": {
            "premium": !premium
          }
        }
      }
    });
  }
  if (!isAdmin) {
    return premium ? "Oui" : "Non"
  }

  return <LoadingButton
    variant="contained"
    color={premium ? "primary" : "secondary"}
    loading={savingRef}
    onClick={switchPremium} >
    {premium ? "Oui" : "Non"}
  </LoadingButton >


}


/**
 * List of all Referentials
 *  
 * This can be used when user create a job to choose the referential
 * 
 * so create `const [selectedIndex, setSelectedIndex] = useState({});`  in parent and pass them to AdminReferentials to enable selection on click
 */
export function ReferentialChooser({isAdmin = false, selectedIndex = null, setSelectedIndex = null, userCanUsePremium = false}) {
  const {t} = useTranslation();
  const classes = useStyles();

  const [gqlFilters, setGqlFilters] = useState([]);
  const columns = getColumns({isAdmin, selectedIndex, setSelectedIndex, classes, t, userCanUsePremium});
  return (
    <CollectionView
      columns={columns}
      gqlConnectionPath={"vocabularies"}
      gqlCountPath={"vocabulariesCount"}
      gqlQuery={gqlReferentials}
      gqlVariables={{}}
      gqlFilters={gqlFilters}
      availableDisplayModes={{"table": {}}}
      searchEnabled={false}
      removalEnabled={false}
    />
  );
}
