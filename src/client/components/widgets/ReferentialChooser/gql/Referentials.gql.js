import {gql} from "@apollo/client";

export const gqlReferentials = gql`
 query Vocabularies($qs: String, $first: Int, $after: String, $sortings: [SortingInput], $filters: [String]) {
  vocabulariesCount(qs: $qs, filters: $filters)
    vocabularies(qs: $qs, first: $first, after: $after, sortings: $sortings, filters: $filters) {  
      edges {
        node {
          id
          uri          
          prefLabel
          altLabel
          comment
					premium
          schemes {
            edges {
              node {
                id
                uri
                prefLabel
                definition
              }
            }
          }
        }
      }
    }
  }
`;

export const gqlUpdateReferential = gql`
  mutation UpdateVocabulary($input: UpdateVocabularyInput!) {
    updateVocabulary(input: $input) {
      updatedObject {
        id
        premium         
      }
    }
  }
`;



