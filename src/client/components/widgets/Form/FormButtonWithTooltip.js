import React from 'react';
import {Button, Tooltip, Typography} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {LoadingButton} from "../../widgets/Button/LoadingButton";

const formLabels = {
  bio: "PERSON.BIOGRAPHY",
  city: "AGENT.ADDRESSES_LABELS.CITY",
  country: "AGENT.ADDRESSES_LABELS.COUNTRY",
  credits: "RESOURCE.CREDITS",
  description: "ORGANIZATION.DESCRIPTION",
  firstName: "PERSON.FIRST_NAME",
  label: "ENTITY.LABEL_TYPE",
  lastName: "PERSON.LAST_NAME",
  link: "ENTITY.LINK",
  name: "ORGANIZATION.NAME",
  number: "AGENT.PHONE_NUMBER",
  startDate: "PROJECT.EVENTS.START_DATE",
  endDate: "PROJECT.EVENTS.END_DATE",
  postalCode: "AGENT.ADDRESSES_LABELS.POSTALCODE",
  shortBio: "PERSON.SHORT_BIOGRAPHY",
  shortDescription: "PROJECT.SHORT_DESCRIPTION",
  street1: "AGENT.ADDRESSES_LABELS.STREET1",
  title: "PROJECT.TITLE",
  email: "SIGN_IN.EMAIL",
  password: "SIGN_IN.PASSWORD"
};

/**
 * hook who check if a form can be submitted and if not return reason 
 * @returns {canSubmit, reason}
 */
export const useCanBeSubmitted = ({label, errors, touched, isValid, dirty, saving, validateOnMount} = {}) => {
  const {t} = useTranslation();

  const canSubmit = isValid && (dirty || (validateOnMount && Object.values(touched || {}).includes(true)));

  let reason;
  if (errors) {
    let required = [];
    let tooShort = [];
    for (let i in errors) {
      if (errors[i] === "Required" || errors[i] === "Champ obligatoire") {
        required.push(t(formLabels[i]) || i);
      } else if (errors[i] === "Too Short!") {
        tooShort.push(t(formLabels[i]) || i);
      }
    }
    if (required.length > 0 || tooShort.length > 0) {
      reason = (
        <div style={{fontSize: 14}}>
          {required.length > 0 && (
            <>
              {t("FORM_ERRORS.FIELD_ERRORS.REQUIRED_S")}
              <ul>
                {required.map((item, index) => (
                  <li key={index}>{item}</li>
                ))}
              </ul>
            </>
          )}
          {tooShort.length > 0 && (
            <>
              {t("FORM_ERRORS.FIELD_ERRORS.TOO_SHORTS")}
              <ul>
                {tooShort.map((item, index) => (
                  <li key={index}>{item}</li>
                ))}
              </ul>
            </>
          )}
        </div>
      );
    }
  } else if (!dirty) {
    reason = (
      <Typography color="inherit">{t("FORM_ERRORS.GENERAL_ERRORS.DISABLED_BECAUSE_NO_MODIFICATION")}</Typography>
    );
  }

  return {canSubmit, reason};
}

/**
 * disabled or not a submit button of a form, display the reason with a tooltip
 * it can be disabled if no edit was done on inputs or required fields are not filled
 * it will be enabled if valuesFromLinkEdit contain any change
 * @param errors
 * @param touched
 * @param isValid
 * @param dirty
 * @param validateOnMount
 * @param {boolean} saving
 */
export const FormButtonWithTooltip = ({label, errors, touched, isValid, dirty, saving, validateOnMount} = {}) => {
  const {t} = useTranslation();
  const {canSubmit, reason} = useCanBeSubmitted({label, errors, touched, isValid, dirty, saving, validateOnMount});

  if (!canSubmit) {
    return (
      <Tooltip title={reason || ""} arrow>
        <span>
          {/* don't remove this span or it will fire an error because of disabled button inside tooltip cannot being able to display title */}
          <Button disabled={true} type="submit" variant="contained" color="primary">
            {label || t("ACTIONS.SAVE")}
          </Button>
        </span>
      </Tooltip>
    );
  } else {
    return (
      <LoadingButton loading={saving} type="submit" variant="contained" color="primary">
        {label || t("ACTIONS.SAVE")}
      </LoadingButton>
    );
  }
};

