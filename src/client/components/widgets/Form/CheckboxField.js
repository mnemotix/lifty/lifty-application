import React from "react";
import {Field} from "formik";
import {CheckboxWithLabel} from "formik-material-ui";

export function CheckboxField(props) {
  return <Field fullWidth color="secondary" id={props.name} component={CheckboxWithLabel} Label={{label: props.label}} {...props} />;
}

