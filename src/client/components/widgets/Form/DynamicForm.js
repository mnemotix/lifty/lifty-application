/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import PropTypes from "prop-types";
import {DynamicFormDefinition, prepareMutation} from "@mnemotix/synaptix-client-toolkit";
import {StoreObject} from "@apollo/client";
import {Prompt} from "react-router-dom";
import {createContext, useContext, useEffect, useState} from "react";
import {Form, Formik} from "formik";
import {FormRowLayout} from "./FormRowLayout";
import {merge, pick, uniqBy, isEqual} from "lodash";
import {useTranslation} from "react-i18next";
import {FormOnTheFlyLayout} from "./FormOnTheFlyLayout";

class DynamicFormController {
  constructor ({
    initialValues = {},
    mutationConfig = {},
    alterInitialValues = () => { },
    alterMutationConfig = () => { }
  } = {}) {
    this._initialValues = initialValues;
    this._mutationConfig = mutationConfig;
    this._alterInitialValues = alterInitialValues;
    this._alterMutationConfig = alterMutationConfig;
  }

  /**
   * @param {object} [values={}]
   */
  alterInitialValues(values = {}) {
    this._alterInitialValues(values);
  }

  /**
   * @param {string[]} [scalarInputNames=[]]
   * @param {LinkInputDefinition[]} [linkInputDefinitions={}]
   */
  alterMutationConfig({scalarInputNames = [], linkInputDefinitions = []} = {}) {
    this._alterMutationConfig({scalarInputNames, linkInputDefinitions});
  }
}

const FormContext = createContext(null);

/**
 * @param initialValues
 * @param setInitialValues
 * @param children
 */
function FormContextProvider({initialValues, mutationConfig, alterInitialValues, alterMutationConfig, children}) {
  return (
    <FormContext.Provider
      value={new DynamicFormController({initialValues, alterInitialValues, mutationConfig, alterMutationConfig})}>
      {children}
    </FormContext.Provider>
  );
}

/**
 * Dynamic Form using Formik to create new item with inputs links
 * 
 * @param {StoreObject} object
 * @param {DynamicFormDefinition} formDefinition
 * @param initialValues
 * @param {function} mutateFunction - Apollo mutation function created by useMutation hook.
 * @param validateOnBlur
 * @param validateOnChange
 * @param enableReinitialize
 * @param {function} [formatMutationInput=({object, objectInput}) => {}] - Programmatically change mutation input just before mutation is fired
 * @param children
 * @param {boolean} [debugOnly] - Log objectInput without launching mutation. Dry run.
 * @return {JSX.Element}
 * @constructor
 */
export function DynamicForm({
  object,
  formDefinition,
  mutateFunction,
  validateOnBlur = true,
  validateOnChange = true,
  validateOnMount = false,
  enableReinitialize = true,
  saving = false,
  children,
  debugOnly = false,
  onTheFly = false,
  initialTouched,
  formatMutationInput
} = {}) {
  const {t} = useTranslation();
  const [initialValues, setInitialValues] = useState(formDefinition.getInitialValues({object}));
  const [mutationConfig, setMutationConfig] = useState(formDefinition.mutationConfig);

  useEffect(() => {
    setInitialValues(formDefinition.getInitialValues({object}));
  }, [object]);

  // compare if initialValues and current values of form is the same 
  // we compare only scalarInputNames because form add others fields like accessPolicy etc
  function isFormEqual(objectA, objectB) {
    if (formDefinition?.["_mutationConfig"]?.["_scalarInputNames"]) {
      return (isEqual(
        pick(objectA, formDefinition?.["_mutationConfig"]?.["_scalarInputNames"]),
        pick(objectB, formDefinition?.["_mutationConfig"]?.["_scalarInputNames"])
      ))
    }
    return isEqual(objectA, objectB);
  }


  const FormLayout = onTheFly ? FormOnTheFlyLayout : FormRowLayout;

  return (
    <FormContextProvider alterInitialValues={alterInitialValues} alterMutationConfig={alterMutationConfig}>
      <Formik
        enableReinitialize={enableReinitialize}
        initialValues={initialValues}
        initialTouched={initialTouched}
        onSubmit={handleSubmit}
        validateOnMount={validateOnMount}
        validateOnChange={validateOnChange}
        validateOnBlur={validateOnBlur}
        validationSchema={formDefinition.getValidationSchema({t})}>
        {({errors, touched, isValid, dirty, submitForm, values, submitCount}) => {
          return (
            <Form>
              <Prompt
                when={dirty && submitCount === 0 && !isFormEqual(initialValues, values)}
                message={t("ACTIONS.CONFIRM_LIVE_FORM")}
              />
              <FormLayout
                sticky={false}
                formSubmitData={{
                  saving,
                  errors,
                  touched,
                  isValid,
                  dirty,
                  submitForm
                }}>
                {children}
              </FormLayout>
            </Form>
          );
        }}
      </Formik>
    </FormContextProvider>
  );

  function alterInitialValues(values) {
    // Warning : leave the callback here to prevent race condition and get previous state.
    // @see https://reactjs.org/docs/hooks-reference.html#functional-updates
    setInitialValues(initialValues => {
      return merge({}, initialValues, values);
    });
  }

  /**
   * @param config
   */
  function alterMutationConfig(config) {
    // Warning : leave the callback here to prevent race condition and get previous state.
    // @see https://reactjs.org/docs/hooks-reference.html#functional-updates
    setMutationConfig(({scalarInputNames, linkInputDefinitions}) => {
      if (config.scalarInputNames) {
        scalarInputNames = (scalarInputNames || []).concat(config.scalarInputNames);
      }

      if (config.linkInputDefinitions) {
        linkInputDefinitions = (linkInputDefinitions || []).concat(config.linkInputDefinitions);
      }

      return {
        scalarInputNames,
        linkInputDefinitions: uniqBy(linkInputDefinitions, "name")
      };
    });
  }

  /**
   * @param mutatedObject
   */
  async function handleSubmit(mutatedObject) {
    const {objectInput, updateCache} = prepareMutation({
      initialObject: initialValues,
      mutatedObject,
      mutationConfig
    });

    if (debugOnly) {
      console.debug({objectInput, mutatedObject, initialObject: initialValues, mutationConfig});
      return;
    }

    if (!formatMutationInput) {
      console.log("no formatMutationInput")
      // Mutation is either a creation or an update.
      // As GraphQL mutations are dynamically generated, we are ensure that they have a consistence input signature.
      // @see Synaptix.js GraphQL documentation.
      formatMutationInput = ({object, objectInput}) => ({
        ...(object?.id ? {objectId: object.id} : null), // In case of update.
        objectInput
      });
    }

    await mutateFunction({
      variables: {
        input: formatMutationInput({object, objectInput})
      },
      update: updateCache
    });
  }
}

DynamicForm.propTypes = {
  mutateFunction: PropTypes.func.isRequired,
  formDefinition: PropTypes.instanceOf(DynamicFormDefinition).isRequired,
  object: PropTypes.object,
  validateOnBlur: PropTypes.bool,
  validateOnChange: PropTypes.bool,
  enableReinitialize: PropTypes.bool,
  saving: PropTypes.bool,
  children: PropTypes.any
};

DynamicForm.defaultProps = {
  validateOnBlur: true,
  validateOnChange: true,
  enableReinitialize: true,
  saving: false
};

/**
 * @return {DynamicFormController}
 */
export function useFormController() {
  return useContext(FormContext);
}
