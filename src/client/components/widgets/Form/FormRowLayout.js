import React, {useEffect} from "react";
import clsx from "clsx";

import {useHistory} from "react-router-dom";
import {useTranslation} from "react-i18next";
import {Grid, Button} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import globalStyles from "../../../globalStyles";
import {FormButtonWithTooltip} from "./FormButtonWithTooltip";
import Config from '../../../Config';

const useStyles = makeStyles((theme) => ({
  sticky: {
    paddingTop: "100px",
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      paddingLeft: "50px",
      position: "fixed"
    }
  },
  cancel: {
    marginRight: theme.spacing(2)
  }
}));

/**
 * display a Grid layout with two rows :
 * the left row will contain a form inputs for example
 * the right contain a sticky 2 buttons to cancel or submit the form
 *
 * @param {*} children Form content to be putted on the left row of the grid
 * @param {*} formContent @deprecated Form content to be putted on the left row of the grid
 * @param {function} onCancelClick : Optional function called on 'cancel' button, 'history.goBack()' is called if param is null
 * @param {object} formSubmitData : {valuesFromLinkEdit, errors, touched, isValid, dirty, label, saving} , props passed to <FormButtonWithTooltip>
 * @param {boolean} sticky : add sticky css to buttons
 */
export function FormRowLayout({formContent, children, onCancelClick, formSubmitData, sticky = true} = {}) {
  const {valuesFromLinkEdit, errors, touched, isValid, dirty, label, saving} = formSubmitData;
  const classes = useStyles();
  const globalClasses = globalStyles();
  let history = useHistory();
  const {t} = useTranslation();

  return (
    <Grid container spacing={2} direction="row" justify="flex-start" alignItems="stretch">
      <Grid item xs={12} md={8}>
        {formContent || children}
      </Grid>


      <Grid item xs={12} md={4}>
        <Grid container spacing={2} direction="row" justify="center" alignItems="stretch">
          <Grid item xs={12} className={clsx(globalClasses.fullHeight, globalClasses.alignCenter)}>
            <div className={clsx(sticky && classes.sticky)}>
              <Button
                className={classes.cancel}
                variant="outlined"
                onClick={() => {
                  if (onCancelClick) {
                    onCancelClick();
                  } else {
                    history.goBack();
                  }
                }}>
                {dirty ? t("ACTIONS.CANCEL") : t("ACTIONS.GO_BACK")}
              </Button>

              <FormButtonWithTooltip
                valuesFromLinkEdit={valuesFromLinkEdit}
                errors={errors}
                touched={touched}
                isValid={isValid}
                dirty={dirty}
                saving={saving}
                label={label}
              />
            </div>
          </Grid>
        </Grid>
      </Grid>

    </Grid>
  );
}
