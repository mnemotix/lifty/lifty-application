import React from 'react';
import {useEffect} from "react";
import {useFormikContext} from "formik";

/**
 * @param children
 * @param formSubmitData
 * @return {*}
 * @constructor
 */
export function FormOnTheFlyLayout({children, formSubmitData} = {}) {
  const {isValid, dirty, saving} = formSubmitData;
  const {submitForm} = useFormikContext();

  useEffect(() => {
    if (isValid && dirty && !saving) {
      (async () => {
        await submitForm()
      })();
    }
  }, [dirty, isValid]);

  return children;
}
