import React from 'react';
import {makeStyles} from "@material-ui/core/styles";
import Alert from '@material-ui/lab/Alert';

const useStyles = makeStyles((theme) => ({
  precode: {
    maxHeight: '350px',
    overflow: 'auto',
    whiteSpace: 'pre-wrap',
  }
}));

export function PreCode({message, error = false}) {

  const classes = useStyles();
  if (error) {
    return <div className={classes.precode}>
      <Alert severity={'error'} >
        <pre><code>{message}</code></pre>
      </Alert>
    </div>
  }
  return <div className={classes.precode}><pre><code>{message}</code></pre></div>
}