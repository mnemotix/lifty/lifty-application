/*
 * Copyright (C) 2013-2020 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, {useEffect, useRef, useState} from "react";
import {useApolloClient, useMutation, useQuery, gql} from "@apollo/client";
import {LoadingSplashScreen} from "../../widgets/LoadingSplashScreen";
import {useTranslation} from "react-i18next";
import get from "lodash/get";
import {makeStyles} from "@material-ui/core/styles";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  IconButton,
  Paper,
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Grid
} from "@material-ui/core";
import MoodBadIcon from "@material-ui/icons/MoodBad";
import Pagination from "@material-ui/lab/Pagination";
import ViewListIcon from "@material-ui/icons/ViewList";
import AppsIcon from "@material-ui/icons/Apps";
import TimelineIcon from '@material-ui/icons/Timeline';
import BurstModeIcon from "@material-ui/icons/BurstMode";
import DeleteIcon from "@material-ui/icons/Delete";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowRightIcon from "@material-ui/icons/KeyboardArrowRight";
import ViewCompactIcon from '@material-ui/icons/ViewCompact';

import URLSearchParams from "@ungap/url-search-params";
import {useLocation, useHistory} from "react-router-dom";
import {useSnackbar} from "notistack";
import Config from '../../../Config';
import {TableView} from "./TableView/TableView";
import globalStyles from "../../../globalStyles";
import {removeNodesInApolloCache} from "../../../utilities/removeNodesInApolloCache";
import {useResponsive} from "../../../hooks/useResponsive";
import {LoadingButton} from "../../widgets/Button/LoadingButton";
import {SearchBar} from "../SearchBar";

const gqlRemoveEntities = gql`
  mutation RemoveEntities($input: RemoveEntitiesInput!) {
    removeEntities(input: $input) {
      deletedIds
    }
  }
`;

export const builtinDisplayModes = {
  table: {
    key: "table",
    Icon: ViewListIcon,
    hidePagination: false,
    renderComponent: (props) => <TableView {...props} />
  }
};

/**
 * @param {object} gqlQuery
 * @param {string} gqlConnectionPath
 * @param {string} gqlCountPath
 * @param {object} [gqlSortings]
 * @param {object} [gqlVariables]
 * @param {object} [gqlFilters]
 * @param {number} [pageSize=12]
 * @param {number} [page=1]
 * @param {string} [qs]
 * @param {object} [availableDisplayModes]  object with available modes to display, can be just keys (use builtinDisplayModes) or extended key/values to overide builtinDisplayModes or to create new mode like timeline
 * @param {string} [displayMode]  default display mode 
 * @param {boolean} [removalEnabled]
 * @param {boolean} [searchEnabled]
 * @param {function} [renderLeftSideActions]
 * @param {function} [renderRightSideActions]
 * @param {function} [onDisplayModeChange]
 * @param {function} [onSelectNodes]
 * @param {bool} multiSelectEnabled : multi select item in table
 * @param {bool} showTopMenu : show or not the top menu with differente mode, left & right actions , hidded for MasonryView et BlogView for exemple
 * @param {bool} hideIfNoResults : hide collection View when no results
 * @return {*}
 * @constructor
 */
export function CollectionView({
  pageSize: defaultPageSize,
  page: defaultPage,
  qs: forceQs,
  displayMode: defaultDisplayMode,
  compact = false,
  gqlQuery,
  gqlConnectionPath,
  gqlCountPath,
  gqlSortings,
  gqlVariables,
  gqlFilters,
  removalEnabled,
  searchEnabled,
  onSelectNodes,
  renderFilters,
  renderLeftSideActions = () => { },
  renderRightSideActions = () => { },
  NoResultComponent,
  availableDisplayModes = {},
  onDisplayModeChange,
  onGqlVariablesChange = () => { },
  multiSelectEnabled = true,
  showTopMenu = true,
  hideIfNoResults = false,
  ...props
}) {
  const globalClasses = globalStyles();
  const classes = useStyles();
  const params = new URLSearchParams(useLocation().search);

  if (params.has("pageSize")) {
    defaultPageSize = parseInt(params.get("pageSize"));
  }

  if (params.has("page")) {
    defaultPage = parseInt(params.get("page"));
  }

  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();
  const apolloClient = useApolloClient();
  const history = useHistory();

  let displayModes = {};
  /*
    availableDisplayModes  can be just keys ` {"table": {default:true}, "grid": {}}` so use definition from builtinDisplayModes
    or it can extended key/values to overide builtinDisplayModes   {"table": { Icon: ....} }
    or to create new mode like timeline
  */
  for (const mode in availableDisplayModes) {
    displayModes[mode] = {
      ...(builtinDisplayModes[mode] || {}),
      ...availableDisplayModes[mode]
    }
    if (!defaultDisplayMode && availableDisplayModes[mode]?.default) {
      defaultDisplayMode = mode;
    }
  }

  // hide collection view when no results, for public mode 
  const [hideCollectionView, setHideCollectionView] = useState(hideIfNoResults);
  // display the mode by default, if many the one with `default:true`  
  const [currentDisplayMode, setCurrentDisplayMode] = useState(displayModes?.[defaultDisplayMode] || displayModes[Object.keys(displayModes)[0]]);

  const [pageSize, setPageSize] = useState(defaultPageSize || 12);
  const [currentPage, setCurrentPage] = useState(defaultPage || 1);
  const [sortings, setSortings] = useState(gqlSortings);
  const [filters, setFilters] = useState(gqlFilters);
  const [selectedNodes, setSelectedNodes] = useState([]);
  const [removeConfirmDialogOpened, setRemoveConfirmDialogOpened] = useState(false);
  const [pageCount, setPageCount] = useState(0);
  const [filtersActive, setFiltersActive] = useState(false);
  const [qs, setQs] = useState(forceQs);
  const {isMobile, isDesktop} = useResponsive();



  useEffect(() => {
    setCurrentPage(defaultPage || 1);
  }, [qs]);

  useEffect(() => {
    setQs(forceQs);
  }, [forceQs]);

  useEffect(() => {
    setFilters(gqlFilters);
  }, [gqlFilters]);

  useEffect(() => {
    if (defaultPageSize) {
      setPageSize(defaultPageSize);
    }
  }, [defaultPageSize]);

  useEffect(() => {
    onGqlVariablesChange(getGqlVariables());
  }, [qs, pageSize, currentPage, sortings, filters, JSON.stringify(gqlVariables)]);

  function getGqlVariables() {
    return {
      qs,
      first: pageSize,
      after: currentPage > 1 ? `offset:${(pageSize - 1) * (currentPage - 1)}` : null,
      sortings,
      filters,
      ...gqlVariables
    };
  }

  let cachedData = useRef(undefined);


  const {data, loading, error} = useQuery(gqlQuery, {
    fetchPolicy: "cache-and-network",
    // @see https://github.com/apollographql/apollo-client/issues/6760#issuecomment-668188727
    nextFetchPolicy: "cache-first",
    errorPolicy: "all",
    variables: getGqlVariables(),
    onCompleted: (data) => { 
      const _count = get(data, gqlCountPath);
      const pageCount = Math.ceil(_count / pageSize);
      if (currentPage > pageCount) {
        handlePageChange(1);
      }
      setHideCollectionView(hideIfNoResults && _count < 1);
      setPageCount(pageCount);
    }
  });

  // @see https://github.com/apollographql/apollo-client/issues/6603#issuecomment-661817678
  if (data) {
    cachedData.current = data;
  }

  const [removeNodes, {loading: savingRemove}] = useMutation(gqlRemoveEntities, {
    onCompleted: async (data) => {
      enqueueSnackbar(t("REMOTE_TABLE.ACTIONS.REMOVE_SUCCESS"), {variant: "success"});
      if (pageCount > 1) {
        await apolloClient.reFetchObservableQueries();
      }
    }
  });

  function handlePageChange(page) {
    setCurrentPage(page);
    params.set("page", page);
    console.log("history replace, #DECOMMENTCODE") // ENABLE_PUBLIC_MODE_ACCESS
    // history.replace({search: params.toString()});
  }

  if (error) {
    console.error(error);
  }

  if (!cachedData.current) {
    return <LoadingSplashScreen />
  } else
    if (hideCollectionView) {
      return null
    } else
      return (
        <div className={classes.collectionViewRoot}>
          {showTopMenu &&
            <Accordion expanded={filtersActive} variant={"outlined"} className={classes.actions}>
              <AccordionSummary>
                <div className={globalClasses.flexSpaceBetween}>
                  {searchEnabled || compact || renderFilters || (Object.keys(availableDisplayModes).length > 1) || renderLeftSideActions &&
                    <div className={globalClasses.flexFS}>
                      <If condition={searchEnabled}>
                        <SearchBar
                          value={qs}
                          onRequestSearch={handleRequestSearch}
                          onCancelSearch={handleCancelSearch}
                          loading={loading}
                          placeholder={t("REMOTE_TABLE.TOOLBAR.SEARCH")}
                        />
                      </If>

                      <If condition={compact}>{renderPagination({key: "compact"})}</If>
                      <If condition={renderFilters}>
                        <Button
                          variant={filtersActive ? "contained" : "outlined"}
                          endIcon={filtersActive ? <KeyboardArrowDownIcon /> : <KeyboardArrowRightIcon />}
                          onClick={() => setFiltersActive(!filtersActive)}>
                          {t("REMOTE_TABLE.ACTIONS.FILTERS")}
                        </Button>
                      </If>
                      <If condition={Object.keys(availableDisplayModes).length > 1}>{renderDisplayButtons()}</If>

                      {renderLeftSideActions()}

                    </div>
                  }
                  {(removalEnabled || renderRightSideActions) &&
                    <div className={classes.actionsRight}>
                      <If condition={removalEnabled}>
                        <LoadingButton
                          variant="contained"
                          startIcon={<DeleteIcon />}
                          color="secondary"
                          disabled={selectedNodes.length === 0}
                          loading={savingRemove}
                          onClick={() => setRemoveConfirmDialogOpened(true)}>
                          {isDesktop ?
                            t("REMOTE_TABLE.ACTIONS.REMOVE", {count: selectedNodes.length})
                            : selectedNodes.length
                          }
                        </LoadingButton>

                        <Dialog open={removeConfirmDialogOpened} onClose={() => setRemoveConfirmDialogOpened(false)}>
                          <DialogContent>
                            <DialogContentText>
                              {t("REMOTE_TABLE.ACTIONS.REMOVE_CONFIRM_TEXT", {count: selectedNodes.length})}
                            </DialogContentText>
                          </DialogContent>
                          <DialogActions>
                            <Button onClick={() => setRemoveConfirmDialogOpened(false)} color="primary">
                              {t("ACTIONS.CANCEL")}
                            </Button>
                            <LoadingButton onClick={onRemoveNodes} color="primary" autoFocus loading={savingRemove}>
                              {t("ACTIONS.PROCEED")}
                            </LoadingButton>
                          </DialogActions>
                        </Dialog>
                      </If>
                      {renderRightSideActions()}
                    </div>
                  }
                </div>
              </AccordionSummary>
              <AccordionDetails>{renderFilters ? renderFilters() : null}</AccordionDetails>
            </Accordion>
          }

          <Choose>
            <When condition={gqlCountPath === null || get(cachedData.current, gqlCountPath) > 0}>
              {renderDisplay()}
              <If condition={!compact}>{renderPagination({key: "full"})}</If>
            </When>
            <Otherwise>
              {NoResultComponent || (
                <Paper variant={"outlined"} className={classes.noMatch}>
                  <Grid container direction="column" alignItems="center">
                    <Grid item xs={12}>
                      <MoodBadIcon fontSize="large" className={classes.noMatchIcon} />{" "}
                      <span className={classes.noMatchText}>{t("REMOTE_TABLE.BODY.NO_MATCH")}</span>
                    </Grid>
                  </Grid>
                </Paper>
              )}
            </Otherwise>
          </Choose>
        </div>
      );

  function renderDisplayButtons() {
    return Object.keys(displayModes).map((keyName) => {
      const {key, Icon} = displayModes[keyName];
      return (
        <IconButton
          key={key}
          color={currentDisplayMode.key === key ? "primary" : "default"}
          onClick={() => handleChangeDisplayMode(displayModes[keyName])}>
          <Icon fontSize={isDesktop ? "large" : "default"} />
        </IconButton>
      )
    })
  }

  function renderDisplay() {
    const {renderComponent, key} = currentDisplayMode;

    if (renderComponent) {
      let displayComponentSharedProps = {
        data: cachedData.current,
        gqlConnectionPath,
        gqlCountPath,
        removalEnabled,
        qs,
        selectedNodes,
        onSelectNodes: (nodes) => {
          setSelectedNodes(nodes);
          if (onSelectNodes) {
            onSelectNodes(nodes);
          }
        },
        onColumnSortChange: (column, isSortDescending) => {
          setSortings([
            {
              sortBy: column.name,
              isSortDescending
            }
          ]);
        },
        key,
        multiSelectEnabled,
        ...currentDisplayMode?.props,
        ...props
      };

      if (currentDisplayMode?.dataForView) {
        displayComponentSharedProps["dataForView"] = currentDisplayMode?.dataForView;
      }

      return renderComponent(displayComponentSharedProps);
    } else {
      throw new Error(`No config found for defaultDisplayMode "${defaultDisplayMode}"...`);
    }
  }

  function renderPagination({key}) {
    return (
      <If condition={!currentDisplayMode?.hidePagination && pageCount > 1}>
        <div className={classes.pagination} key={key}>
          <Pagination
            size={compact || isMobile ? "small" : "large"}
            siblingCount={0}
            page={currentPage}
            count={pageCount}
            onChange={(event, page) => handlePageChange(page)}
          />
        </div>
      </If>
    );
  }

  function handleChangeDisplayMode(displayMode) {
    setCurrentDisplayMode(displayMode);

    if (onDisplayModeChange) {
      onDisplayModeChange(displayMode.key);
    }
  }

  /**
   * Handler to remove nodes remotely and locally.
   */
  async function onRemoveNodes() {
    await removeNodes({
      variables: {
        input: {
          ids: selectedNodes.map(({id}) => id)
        }
      },
      update: (cache) => {
        setSelectedNodes([]);
        if (onSelectNodes) {
          onSelectNodes([]);
        }
        setRemoveConfirmDialogOpened(false);

        removeNodesInApolloCache({
          cache,
          query: gqlQuery,
          variables: getGqlVariables(),
          connectionPathInData: gqlConnectionPath,
          countPathInData: gqlCountPath,
          data: cachedData.current,
          deletedNodeIds: selectedNodes.map(({id}) => id)
        });
      }
    });
  }

  /**
   * In this callback called after a SearchBar update, we need to :
   *  - Update local state
   *  - Save this state in local storage to restore it after a goback routing
   *
   * @param qs
   */
  function handleRequestSearch(qs) {
    setQs(qs);
  }

  /**
   * In this callback called after a SearchBar cancel, we need to :
   */
  function handleCancelSearch() {
    setQs("");
  }
}

const useStyles = makeStyles((theme) => ({
  progressContainer: {
    display: "inline-block",
    position: "relative",
    top: theme.spacing(1),
    left: theme.spacing(2)
  },
  pagination: {
    padding: [[theme.spacing(4), 0]],
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  collectionViewRoot: {
    display: "flex",
    flexDirection: "column",
    width: "100%",
    paddingBottom: theme.spacing(2),
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      marginLeft: 10,
    }
  },
  actions: {
    border: "none",
    backgroundColor: "unset"
  },
  actionsRight: {
    zIndex: 100,
    textAlign: "right",
    "& > *": {
      margin: theme.spacing(0.5)
    }
  },
  noMatch: {
    padding: [[theme.spacing(10), 0]]
  },
  noMatchText: {
    verticalAlign: "middle",
    fontSize: theme.typography.fontSize * 1.1
  },
  noMatchIcon: {
    verticalAlign: "middle",
    marginRight: theme.spacing(0.5)
  },
  filterPopper: {
    width: theme.spacing(50)
  }
}));
