import React from 'react';

import {makeStyles} from '@material-ui/core/styles';
import Pagination from '@material-ui/lab/Pagination';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  centerPagination: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '20px',
    marginBottom: '10px'
  },
  hide: {
    visibility: 'hidden'
  },
  ul: {
    "& .MuiPaginationItem-root": {
      color: "#600C88"
    },
    "& .Mui-selected": {
      color: "#ff2882"
    }
  }
}));

export function Paginator({currentPage, pageCount, setCurrentPage}) {
  const classes = useStyles();
  const hide = parseInt(pageCount) <= 1;

  return (
    <div key="pagination" className={clsx(classes.centerPagination, hide && classes.hide)}>
      <Pagination classes={{ul: classes.ul}} size="medium" siblingCount={0} page={currentPage} count={pageCount} onChange={(event, page) => setCurrentPage(page)} />
    </div>
  );
}