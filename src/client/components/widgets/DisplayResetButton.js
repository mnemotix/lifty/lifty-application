

import React, {useState} from 'react';

import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import {Button} from "@material-ui/core";
import {useMutation} from "@apollo/client";
import {gqlUpdateJob} from "../admin/Jobs/gql/UpdateJob.gql";
import {useSnackbar} from "notistack";

const useStyles = makeStyles((theme) => ({
  m20: {
    margin: '20px 0px !important',
  }
}));


/***
 * if and only if error === liftyextractexception display a button who reset the current job 
 * it's for invalid CSV
 */
export function DisplayResetButton({id, error}) {
  const {t} = useTranslation();
  const classes = useStyles();
  const {enqueueSnackbar} = useSnackbar();

  const [jobReseted, setjobReseted] = useState(false);

  const [updateJob, {loading}] = useMutation(gqlUpdateJob, {
    onCompleted: data => {
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
      setjobReseted(true)
    }
  });

  function resetJobClick() {
    updateJob({
      variables: {
        "input": {
          "objectId": id,
          "objectInput": {
            "indexName": null,
            "dirPath": null,
            "filePath": null,
            "statusInput": {
              "unused": true,
              "pending": false,
              "processing": true,
              "done": false,
              "message": null,
              "error": null
            }
          }
        }
      }
    });
  }

  if (error && error.toLowerCase() === "liftyextractexception") {
    return <Button variant="outlined" color="secondary" disabled={loading || jobReseted} onClick={resetJobClick} className={classes.m20}>
      Recommencer le processus
    </Button>
  } else {
    return null
  }
}
