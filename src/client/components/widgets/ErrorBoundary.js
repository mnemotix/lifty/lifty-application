import React from 'react';

import {Component} from 'react';


export default class ErrorBoundary extends Component {
  constructor (props) {
    super(props);
    this.state = {hasError: false};
  }

  static getDerivedStateFromError(error) {
    return {hasError: true};
  }

  componentDidCatch(error, errorInfo) {
    console.error(error);
    console.error(errorInfo.componentStack);
  }

  render() {
    if (this.state.hasError) {
      return <div  >Un problème technique nous empêche d'afficher ce contenu.</div>;
    }
    return this.props.children;
  }
}
