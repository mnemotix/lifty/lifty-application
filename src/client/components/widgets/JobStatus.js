import React from 'react';
import {useTranslation} from "react-i18next";
import Alert from '@material-ui/lab/Alert';
import {makeStyles} from "@material-ui/core/styles";
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';
import Link from '@material-ui/core/Link';

import {DisplayResetButton} from "./DisplayResetButton";
import {PreCode} from "./PreCode";
import {alertMapping, errorMapping} from "../Jobs/tools";

function notEmpty(str) {
  return (!!str && str.length > 0)
}



function displayError(error) {
  if (notEmpty(error) && errorMapping?.[error.toLowerCase()]) {
    return errorMapping[error.toLowerCase()];
  } else {
    return null
  }
}

const useStyles = makeStyles((theme) => ({
  link: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  }
}));



/**
 * Affiche le status d'un job, done processing pending ... 
 */
export function JobStatus({id, status}) {
  const classes = useStyles();
  const {t} = useTranslation();

  if (!status) {
    return null;
  }

  const {done, pending, unused, processing, message, error} = status;

  let s = "unused";
  if (notEmpty(error)) {
    s = "error";
  } else if (!!pending) {
    s = "pending";
  } else if (!!processing) {
    s = "processing";
  } else if (!!done) {
    s = "done";
  } else if (!!unused) {
    s = "unused";
  }

  const {text, severity} = alertMapping[s];

  // si le job est done est que message contient le path d'ouptut on affiche un lien
  // localhost:3034/output/data/uploads/u2oklkryqac2rb/result.csv
  function renderMessage() {
    if (notEmpty(message)) {
      if (done) {
        let href = null;
        if (done && message.startsWith("/data/uploads/")) {
          href = window.location.origin + "/output/" + message
        }

        if (href) {
          return <Link href={href} className={classes.link}>
            <CloudDownloadIcon />&nbsp;&nbsp;&nbsp; Fichier résultat
          </Link>
        } else {
          return <PreCode message={
            `Erreur de création de lien vers ${message} !
Merci de contacter l'assitance !`} />
        }
      } else {
        return <PreCode message={message} />
      }
    }
  }



  return (
    <Alert severity={severity}>
      <div>
        {t(text)} {displayError(error)}
      </div>
      <DisplayResetButton error={error} id={id} />
      {renderMessage()}
    </Alert>
  )
};


