


import React, {useState} from 'react';
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import {formatRoute} from "react-router-named-routes";
import dayjs from "dayjs";
import {createLink} from "../../../utilities/createLink";
import {CollectionView} from "../../widgets/CollectionView/CollectionView";
import {ROUTES} from "../../../routes";
import {gqlPersonJobs} from "./gql/PersonJobs.gql";
import {JobStatus} from "../../widgets/JobStatus.js"
import {getGqlFilter} from "../../Jobs/tools";
import {JobButtonsForFilter} from "../../Jobs/JobButtonsForFilter"

const useStyles = makeStyles((theme) => ({
  avatarCell: {
    width: theme.spacing(8)
  },
  avatar: {
    textDecoration: "none"
  }
}));
/*
  id
  uri
  indexName
  status {
    done
    pending
    processing
    message
    error					
  }
  filePath
  dirPath
  hasVocabulary {
    id
    title
    description
  }
  createdAt	

 
status: null
*/

function getColumns({isAdmin, classes, t}) {
  return [
    {
      name: "id",
      options: {
        display: "excluded"
      }
    },
    /*{
      name: "uri",
      label: t("JOB.URI"),
    },*/
    {
      name: "createdAt",
      label: t("COMMON.CREATED_AT"),
      transformValue: (value) => dayjs(value).format("L LT"),
      options: {sort: true}
    },
    {
      name: "canUsePremium",
      label: "Premium",
      options: {
        customBodyRender: (canUsePremium, {row}) => {
          return !!canUsePremium ? "Oui" : "";
        }
      }
    },
    isAdmin &&
    {
      name: "path",
      label: "Path",
      options: {
        customBodyRender: (useless, {row}) => {
          return row?.filePath || row?.dirPath || null
        }
      }
    },

    {
      name: "indexName",
      label: "Scheme",
      options: {sort: true}
    },
    /* {
      name: "hasVocabulary.id",
      label: t("JOB.REFERENTIAL"),
      options: {						}
    },
    */
    {
      name: "status",
      label: t("JOB.STATUS"),
      options: {
        customBodyRender: (status, {row}) => {
          return <JobStatus status={status} id={row.id} />
        }
      }
    },

    {
      name: "link",
      label: t("JOB.PUBLIC_URL"),
      options: {
        customBodyRender: (useless, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.JOB, {id: row.id}),
            text: t("COMMON.LINK"),
            target: "_blank"
          });
        }
      }
    }
  ];
}

/**
 * 
 * @param {string} id of the person 
  
 */
export function PersonJobs({isAdmin = false, id}) {
  const {t} = useTranslation();
  const classes = useStyles();

  // filtre on jobs status done processing error unused 
  const [gqlFilters, setGqlFilters] = useState([]);
  function setFilters(filter) {
    setGqlFilters(getGqlFilter(filter))
  }

  const columns = getColumns({isAdmin, classes, t});
  if (!id) {
    return null;
  }
  return (
    <CollectionView
      columns={columns}
      gqlConnectionPath={"person.job"}
      gqlCountPath={"person.jobCount"}
      gqlQuery={gqlPersonJobs}
      gqlVariables={{id}}
      gqlFilters={gqlFilters}
      availableDisplayModes={{"table": {}}}
      //	searchEnabled={true}
      removalEnabled={isAdmin}
      renderLeftSideActions={() =>
        <JobButtonsForFilter setFilters={setFilters} />
      }
    />
  );
}

