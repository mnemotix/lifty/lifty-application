import React, {useState} from 'react';
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import {Typography, Breadcrumbs} from "@material-ui/core";
import Alert from '@material-ui/lab/Alert';
import {formatRoute} from "react-router-named-routes";
import dayjs from "dayjs";
import {createLink} from "../../../utilities/createLink";
import {ROUTES} from "../../../routes";
import {gqlJobs} from "./gql/Jobs.gql";
import {CollectionView} from "../../widgets/CollectionView/CollectionView";
import {JobStatus} from "../../widgets/JobStatus";
import {getGqlFilter} from "../../Jobs/tools";
import {JobButtonsForFilter} from "../../Jobs/JobButtonsForFilter"

const useStyles = makeStyles((theme) => ({
  jobsRoot: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    width: '100%'
  },
  breadcrumbs: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  filters: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center'
  }
}));

function getColumns({classes, t}) {
  return [
    {
      name: "id",
      options: {
        display: "excluded"
      }
    },
    /*{
      name: "uri",
      label: t("JOB.URI"),
    },*/
    {
      name: "createdAt",
      label: t("COMMON.CREATED_AT"),
      transformValue: (value) => dayjs(value).format("L LT"),
      options: {filter: true, sort: true, }
    },
    {
      name: "email",
      label: t("PERSON.EMAIL"),
      options: {
        filter: true,
        sort: true,
        customBodyRender: (useless, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.ADMIN_PERSON, {id: row?.providedby?.id}),
            text: row?.providedby?.mainEmail?.email || ''
          });
        }
      }
    },
    /* {
       name: "dirPath",
       label: "dirPath",
       options: {filter: true, sort: true, }
     },*/
    {
      name: "filePath",
      label: "filePath",
      options: {filter: true, sort: true, }
    },
    {
      name: "indexName",
      label: "indexName",
      options: {filter: true, sort: true, }
    },
    /*{
      name: "hasVocabulary.title",
      label: t("JOB.REFERENTIAL"),
      options: {filter: true, sort: true, }
    },*/
    {
      name: "status",
      label: t("JOB.STATUS"),
      options: {
        customBodyRender: (status, {row}) => {
          return <JobStatus status={status} id={row.id} />
        }
      }
    },

    {
      name: "link",
      label: t("JOB.PUBLIC_URL"),
      options: {
        customBodyRender: (useless, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.JOB, {id: row.id}),
            text: t("COMMON.LINK"),
            target: "_blank"
          });
        }
      }
    }
  ];
}


/**
 * List of all jobs
 *  
 */
export default function AdminJobs() {
  const {t} = useTranslation();
  const classes = useStyles();

  const columns = getColumns({classes, t});

  // filtre on jobs status done processing error unused 
  const [gqlFilters, setGqlFilters] = useState([]);
  function setFilters(filter) {
    setGqlFilters(getGqlFilter(filter))
  }

  return (
    <div className={classes.jobsRoot}>
      <Breadcrumbs className={classes.breadcrumbs}>
        <Typography variant="h4" gutterBottom>
          {t("JOB.LIST") + " :"}
        </Typography>
      </Breadcrumbs>
      <JobButtonsForFilter setFilters={setFilters} />
      <CollectionView
        columns={columns}
        gqlConnectionPath={"recognizers"}
        gqlCountPath={"recognizersCount"}
        gqlQuery={gqlJobs}
        gqlVariables={{}}
        gqlFilters={gqlFilters}
        availableDisplayModes={{"table": {}}}
        searchEnabled={false}
        removalEnabled={true}
      />

    </div>
  );
}

/*query Recognizers($filters: [String]) {
  recognizersCount
  recognizers(filters: $filters) {
    edges {
      node {
        uri
        statusMessage
        providedby {
          id
          mainEmail {
            email
          }
        }
      }
    }
  }
}

{"filters": "statusMessage:*"}*/


