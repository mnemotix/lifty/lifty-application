import {gql} from "@apollo/client";
import {gqlStatusFragment} from "./JobStatus.gql";



export const gqlJobFragment = gql`
  fragment JobFragment on Recognizer {
		id
		uri		
		canUsePremium
		...StatusFragment 
		filePath
		dirPath
		indexName
		hasVocabulary {
			id
			prefLabel        
      altLabel
		}
		createdAt	
	} 
  ${gqlStatusFragment}
`;

export const gqlJob = gql`
  query Recognizer($id: ID! ) {
    recognizer(id: $id) {
			...JobFragment	
			providedby{
				id
				mainEmail {
					email 
				}          
			}
    }
  }
  ${gqlJobFragment}
`;


