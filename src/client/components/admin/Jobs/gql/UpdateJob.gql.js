import {gql} from "@apollo/client";

export const gqlUpdateJob = gql`
  mutation UpdateRecognizer($input: UpdateRecognizerInput!) {
    updateRecognizer(input: $input) {
      updatedObject {
        id
        indexName
        hasVocabulary {
          id
          prefLabel        
          altLabel
        }
      }
    }
  }
`;


/*
    {
      "input": {
        "objectId": "recognizer/bym3ldvhhp17cl",
        "objectInput": {
          "indexName":  "esco-label-role",
          "hasVocabularyInput": {
            "id" : "http://ontology.datasud.fr/openemploi/data/vocabulary/esco"
          }
        }
      }
    }
*/


/**
 fully reset a job
{
  "input": {
    "objectId": "recognizer/xhck630x41otnp",
    "objectInput": {
      "indexName":  null,
      "dirPath":null,
      "filePath":null,
      "statusInput":{
        "unused": true,
        "pending":false,          
        "processing": true,
        "done": false,
        "message": null,
        "error": null
      }
    }
  }
}

 */