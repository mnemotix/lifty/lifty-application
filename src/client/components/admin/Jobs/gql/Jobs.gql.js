import {gql} from "@apollo/client";

import {gqlJobFragment} from "./Job.gql";

export const gqlJobs = gql`
 query Recognizers($qs: String, $first: Int, $after: String, $sortings: [SortingInput], $filters: [String]) {
  recognizersCount(qs: $qs, filters: $filters)
  recognizers(qs: $qs, first: $first, after: $after, sortings: $sortings, filters: $filters) {  
      edges {
        node {
          ...JobFragment	
          providedby{
            id
            mainEmail {
              email 
            }          
          }
        }
      }
    }
  }
  ${gqlJobFragment}
`;