import {gql} from "@apollo/client";

export const gqlStatusFragment = gql`
  fragment StatusFragment on Recognizer {
		status {
			unused
			pending
			processing
			done
			message
			error					
		}
	} 
`;

export const gqlJobStatus = gql`
  query Recognizer($id: ID! ) {
    recognizer(id: $id) {
			...StatusFragment			
    }
  }
  ${gqlStatusFragment}
`;