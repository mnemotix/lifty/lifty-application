import {gql} from "@apollo/client";

import {gqlJobFragment} from "./Job.gql";

export const gqlPersonJobs = gql`
  query Person($id: ID!,$qs: String, $first: Int, $after: String, $sortings: [SortingInput], $filters: [String] ) {
    person(id: $id) {
      jobCount(qs: $qs, filters: $filters)
      job(qs: $qs, first: $first, after: $after, sortings: $sortings, filters: $filters) {
        edges {
          node {
            ...JobFragment
          }
        }
      }
    }
  }
  ${gqlJobFragment}
`;