import React from 'react';
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import {Typography, Breadcrumbs} from "@material-ui/core";


import {ReferentialChooser} from "../../widgets/ReferentialChooser/ReferentialChooser";

// get the last item of a path
function getLastItem(thePath) {
  return thePath.substring(thePath.lastIndexOf('/') + 1)
}

const useStyles = makeStyles((theme) => ({
  referentialsRoot: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    width:'100%'
  },
  breadcrumbs: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },

}));


/**
 * List of all Referentials
 *  
 */
export default function AdminReferentials() {
  const {t} = useTranslation();
  const classes = useStyles();

  return (
    <div className={classes.referentialsRoot}>
      <Breadcrumbs className={classes.breadcrumbs}>
        <Typography variant="h4" gutterBottom>
          {t("REFERENTIAL.LIST") + " :"}
        </Typography>
      </Breadcrumbs>

      <ReferentialChooser
        isAdmin={true}
        selectedIndex={null}
        setSelectedIndex={null}
        userCanUsePremium={null}
      />

    </div>
  );
}
