/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React, {useEffect, useState} from "react";
import {useParams, useHistory} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import {useTranslation} from "react-i18next";
import {Typography, Breadcrumbs, Grid} from "@material-ui/core";
import {useMutation, useLazyQuery} from "@apollo/client";
import {formatRoute} from "react-router-named-routes";
import {useSnackbar} from "notistack";
import {LoadingSplashScreen} from "../../widgets/LoadingSplashScreen";
import {ROUTES} from "../../../routes";
import {createLink} from "../../../utilities/createLink";
import {LoadingButton} from "../../widgets/Button/LoadingButton";
import {gqlPerson} from "./gql/Person.gql";
import {gqlUpdatePerson} from "./gql/UpdatePerson.gql";
import {gqlCreatePerson} from "./gql/CreatePerson.gql";
import {PersonDynamicForm} from "./PersonDynamicForm";
import {PersonJobs} from "../Jobs/PersonJobs";

const useStyles = makeStyles((theme) => ({
  peRoot: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    width: '100%'
  },
  breadcrumbs: {
    marginTop: theme.spacing(2),
    marginBottom: theme.spacing(2)
  },
  spacer:{
    height:'50px'
  }

}));

export default function PersonEdit({disableRedirect = false}) {
  const classes = useStyles();
  const {t} = useTranslation();
  const {enqueueSnackbar} = useSnackbar();
  const history = useHistory();
  let id = false;
  const params = useParams();
  if (params.id) {
    id = decodeURIComponent(params.id);
  }

  const [person, setPerson] = useState(null);
  const [getPerson, {loading}] = useLazyQuery(gqlPerson, {
    variables: {id},
    fetchPolicy: 'cache-and-network',
    onCompleted: ({person}) => {
      setPerson({
        id: person?.id,
        comment: person?.comment,
        createdAt: person?.createdAt,
        firstName: person?.firstName,
        lastName: person?.lastName,
        email: person?.mainEmail?.email,
        emailId: person?.mainEmail?.id
      });
    }
  });

  useEffect(() => {
    if (id) {
      getPerson();
    }
  }, [id]);


  const [mutatePerson, {loading: saving}] = useMutation(id ? gqlUpdatePerson : gqlCreatePerson, {
    onCompleted: data => {
      console.log(data);
      if (data.createPerson?.createdObject?.id) {
        id = data.createPerson.createdObject.id;
      }
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});
      if (!disableRedirect) {
        history.push(formatRoute(ROUTES.ADMIN_PERSON, {id}));
      }
    }
  });

  const [addPersonJob, {loading: savingJob}] = useMutation(gqlUpdatePerson, {
    fetchPolicy: 'no-cache',
    onCompleted: data => {
      console.log(data);
      enqueueSnackbar(t("ACTIONS.SUCCESS"), {variant: "success"});

    }
  });

  function onAddJobClick(premium = false) {
    addPersonJob({
      variables: {
        "input": {
          "objectId": person?.id,
          "objectInput": {
            "jobInput": [
              // {"dirPath": "null"} // object cannot be null, pass anyfield with value
              {
                "canUsePremium": premium,
                "statusInput": {
                  "unused": true,
                  "pending": false,
                  "processing": false,
                  "message": "80%",
                  "error": "",
                  "done": false,
                  "message": ""
                }
              }
            ]
          }
        }
      }
    });
  }


  if (id && loading) {
    return <LoadingSplashScreen />;
  }

  return (
    <div className={classes.peRoot}>
      <Breadcrumbs className={classes.breadcrumbs}>
        {createLink({to: ROUTES.ADMIN_PERSONS, text: t("BREADCRUMB.PERSONS")})}
        <Typography color="textPrimary" >
          {id ? person?.firstName + " " + person?.lastName : t("PERSON.CREATE_TITLE")}
        </Typography>
      </Breadcrumbs>

      <PersonDynamicForm person={person} mutateFunction={mutatePerson} fullForm saving={saving} />
    
      {(person?.id && savingJob) &&
        <LoadingSplashScreen >
          <Typography variant="h4" gutterBottom>
            {t("JOB.CREATING")}
          </Typography>
        </LoadingSplashScreen>
      }
      {(person?.id && !savingJob) && <>
        <Grid container
          direction="row"
          justify="space-between"
          alignItems="center">
          <Grid item xs>
            <Typography variant="h4" gutterBottom>
              {t("JOB.LIST") + " :"}
            </Typography>
          </Grid>
          <Grid item xs>
            <LoadingButton
              variant="contained"
              color="primary"
              loading={savingJob}
              onClick={() => onAddJobClick(false)}>
              {t("JOB.CREATE")}
            </LoadingButton>
          </Grid>
          <Grid item xs>
            <LoadingButton
              variant="contained"
              color="secondary"
              loading={savingJob}
              onClick={() => onAddJobClick(true)}>
              {t("JOB.CREATE_PREMIUM")}
            </LoadingButton>
          </Grid>
        </Grid>
        <div className={classes.spacer} />
        <PersonJobs id={person?.id} isAdmin={true} />
      </>}

    </div>
  );
}