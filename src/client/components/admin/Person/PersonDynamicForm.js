/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import {Typography, Grid} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {PersonFormContent} from "./PersonFormContent";
import {DynamicForm} from "../../widgets/Form/DynamicForm";
import {personFormDefinition} from "./form/Person.form";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  pdfRoot: {
    maxHeight: 'max(250px,25vh)'
  }
}));

/**
 * @return {*}
 * @constructor
 */
export function PersonDynamicForm({person, mutateFunction, saving} = {}) {
  const {t} = useTranslation();
  const classes = useStyles();

  return (
    <div className={classes.pdfRoot}>
      <DynamicForm
        object={person}
        formDefinition={personFormDefinition}
        mutateFunction={mutateFunction}
        saving={saving}
        formatMutationInput={({objectInput}) => {

          let email = {
            "email": objectInput.email,
            "accountName": "main"
          };
          if (person?.emailId) { // if email already exist, just update it by passing his ID 
            email["id"] = person.emailId;
          }

          let formattedInput = {
            objectInput: {
              firstName: objectInput.firstName,
              lastName: objectInput.lastName,
              comment: objectInput.comment,
              "emailAccountInputs": [email]
            }
          };
          if (person?.id) {// if person already exist, just update it by passing his ID 
            formattedInput["objectId"] = person.id;
          }

          return formattedInput;
        }}>
        <PersonFormContent id={person?.id} />
      </DynamicForm>
    </div>
  );
}
/*
{
  input: {
    objectId: 'person/0r4kwf50gx59ia',
    objectInput: {
      emailAccountInputs: [
        {
          email: 'alain.ibrahim@mnemotix.com',
          accountName: 'main'
        }
      ]
    }
  }
}
*/