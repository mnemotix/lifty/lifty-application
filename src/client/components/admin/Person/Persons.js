/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import React from 'react';
import {useTranslation} from "react-i18next";
import {makeStyles} from "@material-ui/core/styles";
import {Typography, Breadcrumbs, Grid} from "@material-ui/core";
import dayjs from "dayjs";
import {formatRoute} from "react-router-named-routes";
import {ROUTES} from "../../../routes";
import {createLink} from "../../../utilities/createLink";
import {CollectionView} from "../../widgets/CollectionView/CollectionView";
import {EnforcedButton} from "../../widgets/Button/EnforcedButton";
import {gqlPersons} from "./gql/Persons.gql";

const useStyles = makeStyles((theme) => ({
  personsRoot: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    width:'100%'
  },
}));

function getColumns({classes, t}) {
  return [
    {
      name: "id",
      options: {
        display: "excluded"
      }
    },
    {
      name: "admin",
      label: t("PERSON.ISADMIN"),
      options: {
        filter: true,
        sort: true,
        customBodyRender: (useless, {row}) => {
          return row?.userAccount?.isAdmin ? "Oui" : "Non"
        }
      }
    },
    {
      name: "createdAt",
      label: t("COMMON.CREATED_AT"),
      transformValue: (value) => dayjs(value).format("L LT"),
      options: {
        sort: true
      }
    },
    {
      name: "email",
      label: t("PERSON.EMAIL"),
      options: {
        filter: true,
        sort: true,
        customBodyRender: (useless, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.ADMIN_PERSON, {id: row.id}),
            text: row?.mainEmail?.email || ''
          });
        }
      }
    },
    {
      name: "comment",
      label: t("PERSON.COMMENT"),
      options: {
        filter: true,
        sort: true,
        customBodyRender: (useless, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.ADMIN_PERSON, {id: row.id}),
            text: row?.comment || ''
          });
        }
      }
    },
    {
      name: "firstName",
      label: t("PERSON.FIRST_NAME"),
      options: {
        filter: true,
        sort: true,
        customBodyRender: (firstName, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.ADMIN_PERSON, {id: row.id}),
            text: firstName
          });
        }
      }
    },
    {
      name: "lastName",
      label: t("PERSON.LAST_NAME"),
      options: {
        filter: true,
        sort: true,
        customBodyRender: (lastName, {row}) => {
          return createLink({
            to: formatRoute(ROUTES.ADMIN_PERSON, {id: row.id}),
            text: lastName
          });
        }
      }
    },
    {
      name: "jobCount",
      label: t("JOB.JOBCOUNT"),
    }

  ];
}

export default function Persons({qs, searchEnabled} = {}) {
  const {t} = useTranslation();
  const classes = useStyles();
  let gqlFilters = [];
  const columns = getColumns({classes, t});

  return (

    <div className={classes.personsRoot}>
      <Breadcrumbs className={classes.breadcrumbs}>
        <Typography variant="h4" gutterBottom>
          {t("PERSON.LIST") + " :"}
        </Typography>
      </Breadcrumbs>

      <CollectionView
        columns={columns}
        gqlConnectionPath={"persons"}
        gqlCountPath={"personsCount"}
        gqlQuery={gqlPersons}
        qs={qs}
        gqlFilters={gqlFilters}
        availableDisplayModes={{"table": {}}}
        searchEnabled={searchEnabled}
        removalEnabled={true}
      />
    </div>
  );
}
{/*renderRightSideActions={() =>
          <EnforcedButton locked={false} to={formatRoute(ROUTES.ADMIN_NEW_PERSON)}>
            {t("PERSON.CREATE")}
          </EnforcedButton>
        */}