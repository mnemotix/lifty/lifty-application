import {gql} from "@apollo/client";


export const gqlPersonFragment = gql`
  fragment PersonFragment on Person {
    id    
    firstName
    lastName
    comment
    createdAt
    mainEmail {
      id
      email
    }
  }
`;

export const gqlPerson = gql`
  query Person($id: ID! ) {
    person(id: $id) {
      ...PersonFragment
           
    }
  }
  ${gqlPersonFragment}
`;

/*  userAccount {
        id
        isDisabled
        isUnregistered
        userId
        userGroups {
          edges {
            node {
              id
              label
            }
          }
        }
      }


    userAccount {
      isAdmin: isInGroup(userGroupId: "user-group/AdministratorGroup")
    }

    */