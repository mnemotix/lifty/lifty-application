import {DynamicFormDefinition, MutationConfig} from "@mnemotix/synaptix-client-toolkit";
import {object, string} from "yup";
import {gqlPersonFragment} from "../gql/Person.gql";

export function getPersonValidationSchema({t}) {
  return object().shape({
    firstName: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED")),
    lastName: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED")),
    comment: string().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED")),
    email: string().email().required(t("FORM_ERRORS.FIELD_ERRORS.REQUIRED"))
  });
}




export const personFormDefinition = new DynamicFormDefinition({
  mutationConfig: new MutationConfig({
    scalarInputNames: ["firstName", "lastName", "comment", "email"],
    gqlFragment: gqlPersonFragment
  }),
  postProcessInitialValues: (object) => (object),
  validationSchema: getPersonValidationSchema
});
