import React from 'react';
import {Grid} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {TextField} from "../../widgets/Form";


export function PersonFormContent({id} = {}) {
  const {t} = useTranslation();

  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <TextField name="firstName" label={t("PERSON.FIRST_NAME")} />
      </Grid>
      <Grid item xs={12}>
        <TextField name="lastName" label={t("PERSON.LAST_NAME")} />
      </Grid>
      <Grid item xs={12}>
        <TextField name="comment" required={true} label={t("PERSON.COMMENT")} />
      </Grid>
      <Grid item xs={12}>
        <TextField name="email" required={true} label={t("PERSON.EMAIL")} />
      </Grid>
    </Grid>
  );
}
