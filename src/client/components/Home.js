import React, {useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Config from '../Config';

const useStyles = makeStyles((theme) => ({
  spacer: {
    paddingTop: Config.logoTop,
    [theme.breakpoints.up(Config.theme.breakpoint)]: {
      paddingTop: Config.logoTop * 3
    }
  },
  alignCenter: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
    width: '100%',
    marginBottom: "50px"
  },
  mainText: {
    fontSize: '4rem',
    fontFamily: 'Lato, sans-serif',
    fontWeight: 'bold',
    paddingTop: '6px',
    width: '100%',
    textAlign: 'left',
    [theme.breakpoints.down('sm')]: {
      fontSize: 'calc(10px + 2vmin)',
      paddingLeft: '12px',
    },
    textAlign: "center",
    textJustify: "inter-word"
  }
}));

export default function Home() {
  const classes = useStyles();
  useEffect(() => {
    document.title = 'Lifty';
  }, []);

  return (
    <div className={classes.alignCenter}>
      <div className={classes.spacer} />
      <div className={classes.mainText}>
        Lifty free demo
      </div>
      <div className={classes.spacer} />
    </div>
  );
}
