import React, {useState, useEffect} from 'react';
import {createMuiTheme, ThemeProvider, makeStyles} from '@material-ui/core/styles';
import {AppBar, Toolbar, IconButton, Typography, MenuItem, Menu} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import clsx from 'clsx';
import {useTranslation} from "react-i18next";
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import MaxWidthCentered from './widgets/MaxWidthCentered';
import FlexboxWrap from './widgets/FlexboxWrap';
import ImportedGraphics from '../ImportedGraphics';
import Config from '../Config';
import globalStyles from '../globalStyles';
import {ROUTES} from '../routes';

const theme = createMuiTheme({
  overrides: {
    MuiPopover: {
      paper: {
        width: '100% !important',
        maxWidth: '100% !important',
        height: '80%',
        maxHeight: 'unset',
        left: '0% !important',
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  appbar: {
    height: Config.header.height,
    alignItems: 'center',
    backgroundColor: 'white',
    color: Config.colors.dark,
    backgroundColor: Config.colors.grey,
    display: 'flex',
    justifyContent: 'center',
  },
  appbarAdmin: {
    borderBottom: `7px solid ${Config.colors.darkBlue}`
  },
  smallAppbar: {
    height: `${Config.header.height / 1.5}px`,
    transition: 'all 400ms ease-out',
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  logo: {
    height: `${Config.header.height / 1.2}px`,
    maxHeight: `${Config.header.logoMaxHeight}px`,
    maxWidth: '65vw',
    margin: 'auto'
  },
  smallLogo: {
    // width: 'min(50vw, 250px)',
    height: 'min(9vw, 66px)'
  },
  menuIcon: {
    width: '10vw',
    height: '10vw',
    padding: '2vw 1vh',
    maxHeight: `${Config.header.height / 2}px`,
    maxWidth: `${Config.header.height / 2}px`,
  },
  sectionDesktop: {
    display: 'flex',
    //flex: 1,
    marginLeft: '15px'
  },
  sectionMobile: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  m5: {
    margin: '5px !important',
  },
  m10: {
    margin: '10px !important',
  },
  flexCenter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    //flex: 1,
  },
  flexLeft: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  entrieLogo: {
    height: `min(5vw,32px)`,
    width: 'auto',
    padding: '0px 10px',
    [theme.breakpoints.down(Config.theme.breakpoint)]: {
      height: `max(4w,25px)`
    },
  },
  nopd: {
    margin: 0,
    padding: 0,
    position: "relative",
    listStyle: "none"
  },
  ahref: {
    listStyle: 'none',    
    textDecoration: 'none',
    border: '1px solid rgba(255, 255, 255, 0)',
    borderRadius: '3px',
    zIndex: '1',
    color: Config.colors.dark,
    '&:hover': {
      color: Config.colors.blue,
    },
    fontSize: '1.5rem',
    lineHeight: '20px',
    fontWeight: '700',
    padding: '10px !important',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  selected: {
    color: `${Config.colors.blue} ! important`
  },
}));



const publicEntries = [
  {
    href: ROUTES.CONCEPT,
    menuTitle: "APP_BAR.MENU.CONCEPT"
  },
  {
    href: "mailto:contact@mnemotix.com",
    menuTitle: "APP_BAR.MENU.CONTACT"
  },
  {
    href: ROUTES.DEMO,
    menuTitle: "APP_BAR.MENU.DEMO"
  }, {
    href: ROUTES.SIGN_IN,
    menuTitle: "APP_BAR.MENU.LOGIN"
  },
];

const userEntries = [
  {
    href: ROUTES.JOBS,
    menuTitle: "APP_BAR.MENU.JOBS"
  },
  {
    href: ROUTES.CONCEPT,
    menuTitle: "APP_BAR.MENU.CONCEPT"
  },
  {
    href: "mailto:contact@mnemotix.com",
    menuTitle: "APP_BAR.MENU.CONTACT"
  },
  {
    href: "#",
    forLogout: true,
    menuTitle: "APP_BAR.MENU.LOGOUT"
  }
];

const adminEntries = [
  {
    href: ROUTES.ADMIN_REFERENTIALS,
    menuTitle: "APP_BAR.MENU.REFERENTIALS"
  },
  {
    href: ROUTES.ADMIN_PERSONS,
    menuTitle: "APP_BAR.MENU.AGENTS"
  },
  {
    href: ROUTES.ADMIN_JOBS,
    menuTitle: "APP_BAR.MENU.JOBS"
  },
  {
    href: "#",
    forLogout: true,
    menuTitle: "APP_BAR.MENU.LOGOUT"
  }
];

/**
 * header avec la barre de navigation
 */
export default function Header({isLogged, isAdmin, isDesktop, logout}) {
  const gS = globalStyles();
  const classes = useStyles();
  const {t} = useTranslation();
  // le menu en mode mobile
  const [mobileAnchorEl, setMobileAnchorEl] = useState(null);
  const entries = isAdmin ? adminEntries : isLogged ? userEntries : publicEntries;

  const handleMobileMenuClose = () => {
    setMobileAnchorEl(null);
  };
  const handleMobileMenuOpen = (event) => {
    setMobileAnchorEl(event.currentTarget);
  };

  useEffect(() => {
    handleMobileMenuClose();
  }, [isDesktop]);

  // au scroll dans la page on reduit la taille du logo principal et la hauteur du header
  const showSmallHeader = useScrollTrigger();

  function createEntry({href, forLogout, menuTitle, target}, keyindex) {
    return (<div className={classes.m5} key={keyindex}>
      <Typography variant="h6" noWrap>
        <a href={href} {...target} onClick={forLogout && logout}
          className={clsx(classes.ahref, location.pathname === href && classes.selected)}>
          {t(menuTitle)}
        </ a>
      </Typography>
    </div>
    )
  }


  const mobileSubMenuId = 'mobile-sub-menu-id';
  // affiche le menu en mode mobile uniquement via popover
  const renderMobileMenu = (
    <ThemeProvider theme={theme}>
      <Menu
        anchorReference="anchorPosition"
        anchorPosition={{top: showSmallHeader ? Config.header.height / 1.5 : Config.header.height, left: 0}}
        id={mobileSubMenuId}
        keepMounted
        anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
        transformOrigin={{vertical: 'top', horizontal: 'left'}}
        open={Boolean(mobileAnchorEl)}
        onClose={handleMobileMenuClose}
      >
        {entries.map((item, index) => (
          <MenuItem key={index} onClick={handleMobileMenuClose}>
            {createEntry(item, "mobile" + index)}
          </MenuItem>
        ))}
      </Menu>
    </ThemeProvider>
  );

  return (
    <>
      <AppBar position="fixed" className={clsx(classes.appbar, isAdmin&& classes.appbarAdmin, showSmallHeader && classes.smallAppbar)}>
        <MaxWidthCentered size='lg'>
          <Toolbar disableGutters={true} className={gS.flexSpaceBetween}>
            <a className={classes.logoContainer} href="/">
              <img alt="Lifty" src={showSmallHeader ? ImportedGraphics.logo : ImportedGraphics.logoBig}
                className={clsx(classes.logo, showSmallHeader && classes.smallLogo)} />
            </a>
            {isDesktop ?
              <div className={classes.sectionDesktop}>
                <FlexboxWrap>
                  {entries.map((item, index) => createEntry(item, "desktop" + index))}
                </FlexboxWrap>
              </div>
              :
              <div className={classes.sectionMobile}>
                <IconButton
                  className={classes.largeIcon}
                  aria-label="show more"
                  aria-controls={mobileSubMenuId}
                  aria-haspopup="true"
                  onClick={handleMobileMenuOpen}
                  color="inherit"
                >
                  <MenuIcon className={classes.menuIcon} />
                </IconButton>
              </div>
            }
          </Toolbar>
        </MaxWidthCentered>
      </AppBar>
      {renderMobileMenu}
    </>
  );
}
