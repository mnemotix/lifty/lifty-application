import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import MaxWidthCentered from '../components/widgets/MaxWidthCentered';
import Config from '../Config';


const useStyles = makeStyles((theme) => ({
  footer: {
    minHeight: Config.footer.height,
    alignItems: 'center',
    backgroundColor: Config.colors.darkBlue,
    color: '#707e8c',
    display: 'flex',
    justifyContent: 'center',
    zIndex: '999',
    width: '100%',
    fontSize: '16px'
  },
  ahref: {
    listStyle: 'none',
    textDecoration: 'none',
    border: '1px solid rgba(255, 255, 255, 0)',
    borderRadius: '3px',
    zIndex: '1', 
    color: '#707e8c',
  },
  centerOrRow: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    width: "100%",
  }, 
}));

export default function Footer({test}) {
  const classes = useStyles();
  return (
    <div className={classes.footer} >
      <MaxWidthCentered>
        <div className={classes.centerOrRow}>
          <a href="/" className={classes.ahref}>
            © {new Date().getFullYear()} mnemotix, tous droits réservés.
          </a>
          {test && 'Connecté sur ' + test }
        </div>
      </MaxWidthCentered>
    </div>
  );
}
