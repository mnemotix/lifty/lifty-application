/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import yargs from "yargs";
import ora from "ora";
import dotenv from "dotenv";
import { initSSOCommand } from "./initSSOCommand";

dotenv.config();
process.env.RABBITMQ_LOG_LEVEL = "DEBUG";

export async function grantUser(){
  let spinner = ora().start();
  spinner.spinner = "clock";

  let {
    username,
    groupId,
    isAdmin,
    revoking,
    dataModelPath,
    environmentPath
  } = yargs
    .usage("yarn sso:user:grant [options] -u [Username] -g [GroupID]")
    .option("u", {
      alias: "username",
      describe: "Username",
      demandOption: true,
      nargs: 1
    })
    .option("g", {
      alias: "groupId",
      describe: "Group ID",
      nargs: 1,
    })
    .option("a", {
      alias: "isAdmin",
      describe: "Is admin",
      default: false,
      nargs: 0,
      type: "boolean"
    })
    .option("r", {
      alias: "revoking",
      describe: "Revoke user from group",
      default: false,
      nargs: 0,
      type: "boolean"
    })
    .option("dm", {
      alias: "dataModelPath",
      describe: "Datamodel file location",
      default: "src/server/datamodel/dataModel.js"
    })
    .option("ep", {
      alias: "environmentPath",
      describe: "Environment file location",
      default: "src/server/config/environment.js"
    })
    .help("h")
    .alias("h", "help")
    .epilog("Copyright Mnemotix 2019")
    .help().argv;

  const { synaptixSession, ssoApiClient} = await initSSOCommand({
    environmentPath,
    dataModelPath
  });

  try {
    const user = await ssoApiClient.getUserByUsername(username);

    spinner.info(`${revoking ? "Revoking" : "Granting"} user ${user.getUsername()} (${user.id}) ${revoking ? "from" : "in"} ${isAdmin ? "Admin group" : `group "${groupId}"`}`);

    await synaptixSession.grantUserInGroup({
      user,
      groupId,
      isAdmin,
      revoking
    });

    spinner.succeed(`User grant success`);
  } catch (e) {
    spinner.fail(e.message);
  }

  process.exit(0);
}
