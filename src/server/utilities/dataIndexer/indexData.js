/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import yargs from "yargs";
import ora from "ora";
import dotenv from "dotenv";
import {
  GraphQLContext,
  initEnvironment,
  LabelDefinition,
  LinkStep, logWarning,
  MnxOntologies,
  PropertyStep,
  UnionStep,
  EntityDefinition
} from "@mnemotix/synaptix.js";
import env from "env-var";

import {
  commonEntityFilter,
  commonFields,
  commonMapping
} from "./connectors/commonFields";
import { generateDatastoreAdapater } from "../../middlewares/generateDatastoreAdapter";
import { Client } from "@elastic/elasticsearch";
import path from "path";
import { generateDataModel } from "../../datamodel/generateDataModel";
import fs from "fs";

dotenv.config();

process.env.UUID = "index-data";
process.env.RABBITMQ_RPC_TIMEOUT = "360000";
// process.env.RABBITMQ_LOG_LEVEL = "DEBUG";

const connectorsKnowMapping = {
  "xsd:date": "date",
  "xsd:dateTime": "date",
  "xsd:int": "integer",
  "xsd:long": "long",
  "xsd:float": "float",
  "xsd:double": "double",
  "xsd:boolean": "boolean",
};

const typeMapping = {
  ...connectorsKnowMapping,
  "xsd:dateTimeStamp": "date"
};

export let indexData = async () => {
  let {
    includedTypes,
    excludedTypes,
    allIndices,
    createPercolators,
    deleteConnectorsOnly,
    deleteConnectorsAndIndicesOnly,
    dataModelPath,
    environmentPath
  } = yargs
    .usage("yarn data:index -dm ./src/server/datamodel/dataModel.js")
    .example("yarn data:index -dm ")
    .option("m", {
      alias: "dataModelPath",
      describe: "Datamodel file location",
      default: "src/server/datamodel/dataModel.js"
    })
    .option("e", {
      alias: "environmentPath",
      describe: "Environment file location",
      default: "src/server/config/environment.js"
    })
    .option("a", {
      alias: "allIndices",
      describe: "(Re-)index all indices",
      default: false,
      nargs: 0,
      type: "boolean"
    })
    .option("d", {
      alias: "deleteConnectorsOnly",
      describe: "Delete connectors (but leave indices alive) without reindexing them",
      default: false,
      nargs: 0,
      type: "boolean"
    })
    .option("D", {
      alias: "deleteConnectorsAndIndicesOnly",
      describe: "Delete connectors AND indices without reindexing them",
      default: false,
      nargs: 0,
      type: "boolean"
    })
    .option("i", {
      alias: "includedTypes",
      default: [],
      describe: "List types to include to (re-)index ",
      type: "array"
    })
    .option("x", {
      alias: "excludedTypes",
      default: [],
      describe: "List types to exclude to (re-)index ",
      type: "array"
    })
    .option("p", {
      alias: "createPercolators",
      describe: "Create percolators ",
      default: false,
      nargs: 0,
      type: "boolean"
    })
    .help("h")
    .alias("h", "help")
    .epilog("Copyright Mnemotix 2019")
    .help().argv;

  const environmentDefinition = require(path.resolve(
    process.cwd(),
    environmentPath
  )).default;
  let extraDataModels;
  let dataModelAbsolutePath = path.resolve(process.cwd(), dataModelPath);

  if (fs.existsSync(dataModelAbsolutePath)) {
    extraDataModels = [
      require(path.resolve(process.cwd(), dataModelAbsolutePath)).dataModel
    ];
  }

  initEnvironment(environmentDefinition);

  const dataModel = generateDataModel({
    extraDataModels,
    environmentDefinition
  });

  const elasticsearchNode = env
    .get("ES_CLUSTER_NODE")
    .required()
    .asString();
  const elasticsearchExternalUri = env
    .get("ES_MASTER_URI")
    .required()
    .asString();
  const elasticsearchBasicAuthUser = env.get("ES_CLUSTER_USER").asString();
  const elasticsearchBasicAuthPassword = env.get("ES_CLUSTER_PWD").asString();
  const indexPrefix = env
    .get("INDEX_PREFIX_TYPES_WITH")
    .required()
    .asString();

  const esClient = new Client({
    node: elasticsearchExternalUri,
    auth: {
      username: elasticsearchBasicAuthUser,
      password: elasticsearchBasicAuthPassword
    },
    maxRetries: 2,
    requestTimeout: 60000,
    sniffOnStart: false
  });

  /** @type {SynaptixDatastoreRdfAdapter} */
  let { datastoreAdapter } = await generateDatastoreAdapater({
    graphMiddlewares: [],
    dataModel
  });
  /** @type {SynaptixDatastoreRdfSession} */
  let synaptixSession = datastoreAdapter.getSession({
    context: new GraphQLContext({
      anonymous: true
    })
  });

  let spinner = ora().start();
  spinner.spinner = "clock";

  let connectors = [];

  // Iteration over model definitions to construct GraphDB connectors
  // @see https://graphdb.ontotext.com/documentation/enterprise/elasticsearch-graphdb-connector.html to get the documentation
  // of a connector structure.
  for (let modelDefinition of dataModel.getModelDefinitions()) {
    let included = includedTypes.includes(modelDefinition.getIndexType());
    let excluded = excludedTypes.includes(modelDefinition.getIndexType());

    if (
      modelDefinition.isIndexed() &&
      (allIndices || (included && !excluded))
    ) {
      let connector = connectors.find(
        ({ name }) => name === `${indexPrefix}${modelDefinition.getIndexType()}`
      );

      if (!connector) {
        connector = {
          name: `${indexPrefix}${modelDefinition.getIndexType()}`,
          fields: [
            {
              fieldName: "types",
              propertyChain: [
                "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
              ],
              analyzed: false,
              multivalued: true
            }
          ],
          mappings: {
            types: {
              type: "keyword"
            },
            ...commonMapping
          },
          types: [],
          elasticsearchNode,
          elasticsearchBasicAuthUser,
          elasticsearchBasicAuthPassword,
          elasticsearchClusterSniff: false,
          bulkUpdateBatchSize: 500,
          indexCreateSettings: {
            "index.blocks.read_only_allow_delete": null
          },
          manageMapping: false,
          manageIndex: false
        };


        // If a model definition is a descendant of EntityDefinition, related instances should be traked with actions.
        // @see https://mnemotix.gitlab.io/mnx-models/#/Action
        // Therefore instances marked as deleted (that's to say instances whose a mnx:Deletion is atteched) should be filtered from indexation.
        if (modelDefinition.isEqualOrDescendantOf(EntityDefinition)) {
          connector.fields = [].concat(connector.fields, commonFields);
          connector.entityFilter = commonEntityFilter;
        }

        connectors.push(connector);
      }

      // Map the list of model definition RDF types in the dedicated GraphDB connector types filter.
      // @see https://graphdb.ontotext.com/documentation/enterprise/elasticsearch-graphdb-connector.html#using-the-create-command
      connector.types.push(
        synaptixSession.normalizeAbsoluteUri({
          uri: modelDefinition.getRdfType()
        })
      );

      // Iterate over properties and map them in the dedicated GraphDB connector field.
      // RDF datatype is mapped in ES field type.
      for (let property of modelDefinition.getProperties()) {
        let fieldName = property.getPathInIndex();

        if(typeof fieldName !== "string"){
          continue;
        }

        let dataProperty = property.getRdfDataProperty();
        let dataType = property.getRdfDataType();
        let defaultValue;
        let propertyChain = [];
        let composedField = property.getLinkPath()?.getLastStep() instanceof UnionStep;
        let isLocalized = property instanceof LabelDefinition;
        let multivalued = isLocalized || property.isPlural?.();
        let analyzed = isLocalized || property.isSearchable();

        // Handles the straigh case.
        if (property.getRdfDataProperty()) {
          propertyChain.push(
            synaptixSession.normalizeAbsoluteUri({ uri: dataProperty })
          );
        // Handles the linkPath case.
        } else if (property.getLinkPath()) {
          propertyChain = linkPathToPropertyChain({
            modelDefinition,
            linkPath: property.getLinkPath(),
            synaptixSession
          });

          if( composedField ){
            dataType = property.getLinkPath().getLastStep().getLinkPaths()[0].getLastPropertyStep().getPropertyDefinition().getRdfDataType()
          } else {
            dataType = property.getLinkPath().getLastPropertyStep().getPropertyDefinition().getRdfDataType();
          }
        }

        // Map the datatype
        dataType = dataType
          .replace("http://www.w3.org/2001/XMLSchema#", "xsd:")
          .replace("integer", "int")

        // Map a default value
        if(property.getDefaultValue() != null){
          defaultValue = `\\"${property.getDefaultValue()}\\"^^${dataType}`;
        }

        if (propertyChain.length > 0) {
          // Multiple propertyChains
          if (composedField) {
            for (const [index, fragmentPropertyChain] of Object.entries(propertyChain)) {
              buildPropertyField({connector, fieldName, fieldNameSuffix: `$${parseInt(index) + 1}`, propertyChain: fragmentPropertyChain, property, analyzed, defaultValue, dataType, multivalued, isLocalized});
            }
          } else {
            buildPropertyField({connector, fieldName, propertyChain, property, analyzed, defaultValue, multivalued, isLocalized, dataType});
          }
        }
      }

      // Iterate over links and map them in the dedicated GraphDB connector field.
      for (let link of modelDefinition.getLinks()) {
        let fieldName = link.getPathInIndex();
        let composedField = link.getLinkPath()?.getLastStep() instanceof UnionStep;

        if(typeof fieldName !== "string"){
          continue;
        }

        let objectProperty =
          link.getRdfObjectProperty() ||
          link.getSymmetricLinkDefinition()?.getRdfReversedObjectProperty();
        let linkPath = link.getLinkPath();
        let propertyChain = [];

        // Handles the straigh case.
        if (objectProperty) {
          propertyChain.push(
            synaptixSession.normalizeAbsoluteUri({
              uri: objectProperty
            })
          );
        // Handles the link path case.
        } else if (linkPath) {
          propertyChain = linkPathToPropertyChain({
            modelDefinition,
            linkPath,
            synaptixSession
          });
        } else {
          // logWarning(`Model definition link ${modelDefinition.name} -> ${fieldName} can't be indexed while GraphDB only support straight property chains. Try to change "rdfReversedObjectProperty" (${link.getRdfReversedObjectProperty()}) of link "${fieldName}" by it's owl:inverseOf in "rdfObjectProperty"`);
        }

        if (propertyChain.length > 0) {
          if (composedField) {
            for (const [index, fragmentPropertyChain] of Object.entries(propertyChain)) {
              buildPropertyField({
                connector,
                fieldName,
                fieldNameSuffix: `$${parseInt(index) + 1}`,
                propertyChain:  fragmentPropertyChain,
                analyzed: false,
                multivalued: link.isPlural()
              })
            }
          } else {
            buildPropertyField({
              connector,
              fieldName,
              propertyChain,
              analyzed: false,
              multivalued: link.isPlural()
            })
          }


          connector.mappings[fieldName] = {
            type: "keyword"
          };
        }
      }
    }
  }

  //
  // Lifty special usecase
  //
  // In addition to generic connectors, Lifty needs a fine grain indexation of skos:Concept. Several indices of skos:Concept filtered by their skos:ConceptScheme must be created.
  // Following indices convention is :
  //   `${indexPrefix}${ConceptDefinition.getIndexType()}-${Related vocabulary URI last fragment}-${Related scheme URI last fragment}`
  // For example : "dev-lifty-concept-esco-skills"
  spinner.info(`Creating Lifty (Bigup !) connectors...`);
  spinner.info(`Well give schemes...`);

  const SchemeDefinition = MnxOntologies.mnxSkos.ModelDefinitions.SchemeDefinition;
  const ConceptDefinition = MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition;

  const conceptDefaultConnector = connectors.find(({name}) => name === `${indexPrefix}${ConceptDefinition.getIndexType()}`)

  const schemes =  await synaptixSession
    .getGraphControllerService()
    .getGraphControllerPublisher()
    .construct({
      query: `
PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

CONSTRUCT {
    ?schemeUri mnx:schemeOf ?thesoUri
} WHERE {
    ?schemeUri a skos:ConceptScheme.
    ?schemeUri mnx:schemeOf ?thesoUri
}
`
    });
  spinner.info(`Found ${schemes.length} !`);

  for(const scheme of schemes){
    const schemeUri = scheme["@id"];
    const vocabularyUri = scheme['http://ns.mnemotix.com/ontologies/2019/8/generic-model/schemeOf']?.[0]?.["@id"];

    if (schemeUri && vocabularyUri){
      const indexName = `${indexPrefix}${SchemeDefinition.getIndexType()}-${vocabularyUri.slice(vocabularyUri.lastIndexOf("/") + 1)}-${schemeUri.slice(schemeUri.lastIndexOf("/") + 1)}`;
      spinner.info(`Adding given index "${indexName}" that filters only concept from scheme <${schemeUri}>...`);
      connectors.push({
        ...conceptDefaultConnector,
        name: indexName,
        entityFilter: `bound(?${ConceptDefinition.getLink("inScheme").getPathInIndex()}) && ?${ConceptDefinition.getLink("inScheme").getPathInIndex()} in (<${schemeUri}>)`
      })
    }
  }

  for (let connector of connectors) {
    let name = connector.name;
    let mappings = connector.mappings;
    delete connector.name;
    delete connector.mappings;

    spinner.info(`Switching to index: "${name}"`);

    let [fields] = connector.fields.reduce(
      ([fields, fieldNames], field) => {
        let fieldName = field.fieldName;
        if (!fieldNames.includes(fieldName)) {
          fields.push(field);
          fieldNames.push(fieldName);
        }
        return [fields, fieldNames];
      },
      [[], []]
    );

    connector.fields = fields;

    spinner.info(`Removing existing connector is exists.`);

    // Connector first.
    try {
      await synaptixSession
        .getGraphControllerService()
        .getGraphControllerPublisher()
        .insertTriples({
          query: `PREFIX :<http://www.ontotext.com/connectors/elasticsearch#>
PREFIX inst:<http://www.ontotext.com/connectors/elasticsearch/instance#>
INSERT DATA {
  inst:${name} :dropConnector "" .
}
`
        });
    } catch (e) {}

    if(!deleteConnectorsOnly || deleteConnectorsAndIndicesOnly){
      // Ensure index is deleted as well
      try {
        spinner.info(`Removing index "${name}".`);
        await esClient.indices.delete({
          index: name
        });
      } catch (e) {}
    }

    if (!deleteConnectorsOnly && !deleteConnectorsAndIndicesOnly) {
      try {
        spinner.info(`Creating index "${name}".`);

        await esClient.indices.create({
          index: name,
          body: {
            settings: {
              analysis: {
                filter: {
                  autocomplete_filter: {
                    type: "edge_ngram",
                    min_gram: 1,
                    max_gram: 60
                  }
                },
                analyzer: {
                  autocomplete: {
                    type: "custom",
                    tokenizer: "standard",
                    filter: ["lowercase", "autocomplete_filter"]
                  },
                  french: {
                    type: "standard",
                    stopwords: "_french_"
                  }
                },
                normalizer: {
                  lowercase_no_accent: {
                    "type": "custom",
                    "filter": ["lowercase", "asciifolding"]
                  }
                }
              }
            },
            mappings: {
              properties: mappings
            }
          }
        });
      } catch (e) {
        spinner.fail(e.message);
      }

      try {
        spinner.info(`Creating connector "${name}".`);
        await synaptixSession
          .getGraphControllerService()
          .getGraphControllerPublisher()
          .insertTriples({
            query: `PREFIX :<http://www.ontotext.com/connectors/elasticsearch#>
PREFIX inst:<http://www.ontotext.com/connectors/elasticsearch/instance#>
INSERT DATA {
  inst:${name} :createConnector '''
  ${JSON.stringify(connector, null, " ")}
'''.
}
`
          });
      } catch (e) {
        spinner.fail(e.message);
      }
    }
  }

  if (createPercolators) {
    const sourceIndex = `${indexPrefix}${MnxOntologies.mnxSkos.ModelDefinitions.ConceptDefinition.getIndexType()}`;
    const index = `${sourceIndex}-perco`;
    spinner.start(`Creating percolators on ${index}`);

    try {
      await esClient.indices.delete({
        index
      });
    } catch (e) {}

    try {
      await esClient.indices.create({
        index,
        body: {
          settings: {
            analysis: {
              analyzer: {
                stemmer_analyzer: {
                  tokenizer: "whitespace",
                  filter: ["lowercase", "french_stemmer", "french_stop"]
                }
              },
              filter: {
                french_stemmer: {
                  type: "stemmer",
                  language: "light_french"
                },
                french_stop: {
                  type: "stop",
                  stopwords: "_french_"
                }
              }
            }
          },
          mappings: {
            properties: {
              query: {
                type: "percolator"
              },
              concept_id: {
                type: "text"
              },
              concept_prefLabel: {
                type: "text"
              },
              document: {
                type: "text",
                analyzer: "stemmer_analyzer",
                term_vector: "with_positions_offsets"
              }
            }
          }
        }
      });
    } catch (e) {
      spinner.fail(e.message);
    }

    let {
      body: { count }
    } = await esClient.count({
      index: sourceIndex,
      body: { query: { exists: { field: "hasVocabulary" } } }
    });

    for (let i = 1; i <= count; i++) {
      spinner.text = `Process concept ${i}/${count}`;
      const response = await esClient.search({
        index: sourceIndex,
        from: i - 1,
        size: 1,
        body: { query: { exists: { field: "hasVocabulary" } } }
      });
      const { _id: id, _source: concept } = response.body.hits.hits[0];
      try {
        await esClient.index({
          index,
          id: `${id}/percolator`,
          body: {
            concept_id: id,
            concept_prefLabel: concept.prefLabel,
            query: {
              query_string: {
                query: concept.prefLabel
              }
            }
          }
        });
      } catch (e) {
        spinner.fail(e?.meta?.body?.error || e.message);
      }
    }
    spinner.succeed();
  }

  process.exit(0);
};


/**
 * Build connector a property field
 * @param connector
 * @param fieldName
 * @param fieldNameSuffix
 * @param dataType
 * @param analyzed
 * @param multivalued
 * @param propertyChain
 * @param defaultValue
 * @param isLocalized
 */
function buildPropertyField({connector, fieldName, fieldNameSuffix = "", dataType, analyzed, multivalued, propertyChain, defaultValue, isLocalized }) {

  connector.fields.push({
    fieldName: `${fieldName}${fieldNameSuffix}`,
    propertyChain,
    analyzed,
    multivalued,
    datatype: Object.keys(connectorsKnowMapping).includes(dataType)
      ? dataType
      : null,
    ...(defaultValue ? {defaultValue} : {})
  });

  if (analyzed) {
    connector.mappings[fieldName] = {
      type: "text",
      analyzer: "autocomplete",
      search_analyzer: "autocomplete",
      fields: {
        keyword: (typeMapping[dataType] ? {
          type: typeMapping[dataType]
        } : {
          type: "keyword",
          ignore_above: 256,
          normalizer: "lowercase_no_accent"
        }),
        keyword_case_sentitive: {
          type: "keyword"
        }
      }
    };
  } else {
    connector.mappings[fieldName] = typeMapping[dataType] ? {
      type: typeMapping[dataType]
    } : {
      type: "keyword",
      ignore_above: 256,
      normalizer: "lowercase_no_accent",
      fields: {
        keyword_case_sentitive: {
          type: "keyword"
        }
      }
    };
  }

  if (isLocalized) {

    connector.fields.push({
      fieldName: `${fieldName}_locales${fieldNameSuffix}`,
      propertyChain: [...propertyChain, "lang()"],
      analyzed: false,
      multivalued
    });

    connector.mappings[`${fieldName}_locales`] = {
      type: "keyword"
    };
  }
}

function linkPathToPropertyChain({ modelDefinition, linkPath, synaptixSession }) {
  return linkPath.getSteps().reduce((propertyChain, step) => {
    if (step instanceof LinkStep) {
      let objectProperty =
        step.getLinkDefinition().getRdfObjectProperty() ||
        step.getLinkDefinition().getSymmetricLinkDefinition()?.getRdfReversedObjectProperty();

      if (objectProperty) {
        propertyChain.push(
          synaptixSession.normalizeAbsoluteUri({
            uri: objectProperty
          })
        );
      } else {
        logWarning(`Model definition link ${modelDefinition.name} -> ${linkPath} can't be indexed while GraphDB only support straight property chains. Try to change "rdfReversedObjectProperty" (${step.getLinkDefinition().getRdfReversedObjectProperty()}) of step link "${step.getLinkDefinition().getLinkName()}" by it's owl:inverseOf in "rdfObjectProperty"`);
      }
    } else if (step instanceof PropertyStep) {
      if(step.getPropertyDefinition().getRdfDataProperty()){
        propertyChain.push(
          synaptixSession.normalizeAbsoluteUri({
            uri: step.getPropertyDefinition().getRdfDataProperty()
          })
        );
      } else {
        logWarning(`Linkpath ${linkPath} might be not indexed because property ${step.getPropertyDefinition()} has no getRdfDataProperty() value`)
      }
    } else if (step instanceof UnionStep) {
      for(const linkPath of step.getLinkPaths()){
        propertyChain.push(linkPathToPropertyChain({linkPath, synaptixSession, modelDefinition}))
      }
    }

    return propertyChain;
  }, []);
}
