/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import {
  ModelDefinitionAbstract,
  LinkDefinition, LiteralDefinition,
  GraphQLTypeDefinition,
  MnxOntologies
} from "@mnemotix/synaptix.js";
import RecognizerDefinition from "./RecognizerDefinition";


export default class PersonDefinition extends ModelDefinitionAbstract {

  /**
   * @inheritDoc
   */
  static substituteModelDefinition() {
    return MnxOntologies.mnxAgent.ModelDefinitions.PersonDefinition;
  }

  /**
   * @inheritDoc
   */
  static getRdfType() {  // 
    return "mnx:Person";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() { // dans ES 
    return "agent";
  }


  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  static getLinks() {
    return [
      ...super.getLinks(),
      new LinkDefinition({
        linkName: "hasJob",
        description: "Liste des jobs",
        rdfObjectProperty: "lifty:hasJob",
        relatedModelDefinition: RecognizerDefinition,
        isPlural: true,
        graphQLPropertyName: "job",
        graphQLInputName: "jobInput",
      }),
    ];
  }

  /*
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'organizationName',
        rdfDataProperty: "lifty:name",
        // rdfDataType: "mnx:string"
      }),
    ];
  }
  */
}
