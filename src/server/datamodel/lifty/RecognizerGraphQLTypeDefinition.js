import {GraphQLTypeDefinition, GraphQLUpdateMutation} from "@mnemotix/synaptix.js";
import {createWriteStream, mkdirSync} from "fs";
import merge from "lodash/merge";

import RecognizerDefinition from "./RecognizerDefinition";

const storeUpload = async ({objectId, stream, filename, mimetype}) => {
  const id = objectId.replace('recognizer/', '');
  let dirPath = `data/uploads/${id}/`;
  const filePath = dirPath + filename;

  await mkdirSync(dirPath, {recursive: true}, (err) => {
    if (err) throw err;
  });

  // (createWriteStream) writes our file to the images directory
  return new Promise((resolve, reject) =>
    stream
      .pipe(createWriteStream(filePath))
      .on("finish", () => resolve({objectId, dirPath, filePath}))
      .on("error", reject)
  );
};

const processUpload = async (objectId, upload) => {
  const {createReadStream, filename, mimetype} = await upload;
  const stream = createReadStream();
  const file = await storeUpload({objectId, stream, filename, mimetype});
  return file;
};

export default class RecognizerGraphQLTypeDefinition extends GraphQLTypeDefinition {

  static getUpdateMutation() {
    return new GraphQLUpdateMutation({
      extraInputArgs: `
          """ File to process """
          file: Upload
          """ Scheme to analyze """
          schemeId: ID
		`,
      /** 
        * @param {string} id
        * @param {Upload} file
        * @param {string} schemeId
        * @param {typeof ModelDefinitionAbstract} modelDefinition
        */
      resolver: async ({objectId, file, schemeId, objectInput}, synaptixSession, info) => {
        let recognizerId = synaptixSession.extractIdFromGlobalId(objectId);
        let upload = false;
        if (file) { // only on file upload Process upload and update objectInput
          upload = await processUpload(objectId, file);

          objectInput = merge(objectInput, {
            "indexName": schemeId,
            "statusInput": {
              "unused": false,
              "pending": true
            },
            "dirPath": upload.dirPath,
            "filePath": upload.filePath
          })
        }


        const resUpdate = await synaptixSession.updateObject({
          modelDefinition: RecognizerDefinition,
          objectId: recognizerId,
          updatingProps: objectInput,
          lang: synaptixSession.getContext().getLang()
        });

        if (file) { // send amqp message only on file upload
          
          
          console.log("--->                          send message to lifty.recogs.csv")

          synaptixSession.getDataPublisher().publish("lifty.recogs.csv", {
            "indexName": schemeId,
            "dirPath": upload.dirPath,
            "filePath": upload.filePath,
            "recognizerId": recognizerId
          }, {}, false);
        }

        return resUpdate;

      }
    });
  }
}