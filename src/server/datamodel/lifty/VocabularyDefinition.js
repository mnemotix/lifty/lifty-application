import {
  ModelDefinitionAbstract,
  LiteralDefinition,
  LinkDefinition, LinkPath,
  GraphQLTypeDefinition,
  MnxOntologies
} from "@mnemotix/synaptix.js";


export default class VocabularyDefinition extends ModelDefinitionAbstract {

  /**
   * @inheritDoc
   */
  static substituteModelDefinition() {
    return MnxOntologies.mnxSkos.ModelDefinitions.VocabularyDefinition
  }

  /**
 * @inheritDoc
 */
  static getRdfType() {
    return "lifty:Vocabulary";
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return 'vocabulary';
  }

  /**
 * @inheritDoc
 */
  static getGraphQLDefinition() {
    return MnxOntologies.mnxSkos.ModelDefinitions.VocabularyDefinition.getGraphQLDefinition();
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'premium',
        description: "This vocabulary is a premium feature",
        rdfDataProperty: "lifty:premium",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean"
      }),
    ];
  }

}
