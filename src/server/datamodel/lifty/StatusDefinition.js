/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
import {
  ModelDefinitionAbstract,
  LiteralDefinition,
  GraphQLTypeDefinition,
  EntityDefinition
} from "@mnemotix/synaptix.js";

/*
  { 
    processing : bool,
    pending: bool,
    done: bool,
    error : string,
    message : string
  }
*/

export default class StatusDefinition extends ModelDefinitionAbstract {
  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "lifty:JobStatus";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "jobstatus";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    return GraphQLTypeDefinition;
  }


  static getParentDefinitions() {
    return [EntityDefinition];
  }


  /**
  * @inheritDoc
  */
  static getLinks() {
    return [
      ...super.getLinks()
    ];
  }

  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: 'unused',
        description: "This step is unused, user didn't add csv yet ",
        rdfDataProperty: "lifty:unused",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean",
        //rdfDataType: 'xsd:boolean'
      }),
      new LiteralDefinition({
        literalName: 'pending',
        description: "This step is pending",
        rdfDataProperty: "lifty:pending",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean",
        //rdfDataType: 'xsd:boolean'
      }),
      new LiteralDefinition({
        literalName: 'processing',
        description: "This step is processing",
        rdfDataProperty: "lifty:processing",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean",
        //rdfDataType: 'xsd:boolean'
      }),
      new LiteralDefinition({
        literalName: 'done',
        description: "This step is done",
        rdfDataProperty: "lifty:done",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean",
        //rdfDataType: 'xsd:boolean'
      }),
      new LiteralDefinition({
        literalName: 'message',
        rdfDataProperty: "lifty:message",
        description: "More info about the current step, progress percentage for example"
      }),
      new LiteralDefinition({
        literalName: 'error',
        rdfDataProperty: "lifty:error",
        description: "Error description"
      }),
    ];
  }
}