/*
 * Copyright (C) 2013-2018 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import {
  ModelDefinitionAbstract,
  LiteralDefinition,
  LinkDefinition, LinkPath,
  GraphQLTypeDefinition,
  MnxOntologies
} from "@mnemotix/synaptix.js";

import PersonDefinition from "./PersonDefinition";
import VocabularyDefinition from "./VocabularyDefinition";
import StatusDefinition from "./StatusDefinition"
import RecognizerGraphQLTypeDefinition from "./RecognizerGraphQLTypeDefinition";

/*
  uuid : uri,
  Recognizer: {

    referentiel: string,
    dirPath: string,
    filePath: string,
    indexName: string,
    Status { 
      processing : bool,
      pending: bool,
      done: bool,
      error : string,
      message : string,
    }
    createdAt
    endAt,
    Person
  }
*/
export default class RecognizerDefinition extends ModelDefinitionAbstract {


  /**
   * @inheritDoc
   */
  static getRdfType() {
    return "lifty:Recognizer";
  }

  /**
   * @inheritDoc
   */
  static getGraphQLDefinition() {
    // return GraphQLTypeDefinition;
    return RecognizerGraphQLTypeDefinition;
  }

  /**
   * @inheritDoc
   */
  static getIndexType() {
    return "recognizer";
  }

  static getParentDefinitions() {
    return [MnxOntologies.mnxTime.ModelDefinitions.DurationDefinition];
  }

  /**
   * @inheritDoc
   */
  static getLinks() {
    return [
      ...super.getLinks(),
      // referentiel du job			 
      new LinkDefinition({
        linkName: "hasVocabulary",
        rdfObjectProperty: "lifty:hasVocabulary",
        relatedModelDefinition: VocabularyDefinition,
        //relatedModelDefinition: MnxOntologies.mnxSkos.ModelDefinitions.VocabularyDefinition, 
        isPlural: false,
        graphQLInputName: "hasVocabularyInput",
        graphQLPropertyName: "hasVocabulary"
      }),
      // relation inverse, à quelle personne appartient le job
      new LinkDefinition({
        linkName: "isProvidedBy",
        rdfObjectProperty: "lifty:isProvidedBy",
        relatedModelDefinition: PersonDefinition,
        symmetricLinkName: 'hasJob',
        isCascadingRemoved: true, // remove all task of a person if person is deleted ?
        isPlural: false,
        graphQLInputName: "isProvidedByInput",
        graphQLPropertyName: "providedby"
      }),
      new LinkDefinition({
        linkName: "status",
        rdfObjectProperty: "lifty:status",
        relatedModelDefinition: StatusDefinition,
        isPlural: false,
        graphQLInputName: "statusInput",
        graphQLPropertyName: "status"
      }),

    ];
  }



  /**
   * @inheritDoc
   */
  static getLiterals() {
    return [
      ...super.getLiterals(),
      new LiteralDefinition({
        literalName: "canUsePremium",
        description: "Can the user use Premium Vocabulary for this job ? ",
        rdfDataProperty: "lifty:canusepremium",
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean",
      }),
      new LiteralDefinition({
        literalName: 'endAt',
        rdfDataType: 'http://www.w3.org/2001/XMLSchema#dateTimeStamp'
      }),
      // si le job porte sur plusieurs fichiers il seront dans ce dossier, qui contiendra également le resultat 
      new LiteralDefinition({
        literalName: 'dirPath',
        rdfDataProperty: "mnx:dirPath",
        //rdfDataType: "mnx:string"
      }),
      // si le job ne porte que sur un seul fichier il sera stocké dans filePath
      new LiteralDefinition({
        literalName: 'filePath',
        rdfDataProperty: "mnx:filePath",
        //rdfDataType: "mnx:string"
      }),
      new LiteralDefinition({
        literalName: 'indexName',
        rdfDataProperty: "lifty:indexName",
        // rdfDataType: "mnx:string"
      }),
      new LiteralDefinition({
        literalName: "statusUnused",
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("status")})
          .property({propertyDefinition: StatusDefinition.getLiteral("unused")}),
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean"
      }),
      new LiteralDefinition({
        literalName: "statusPending",
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("status")})
          .property({propertyDefinition: StatusDefinition.getLiteral("pending")}),
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean"
      }),
      new LiteralDefinition({
        literalName: "statusProcessing",
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("status")})
          .property({propertyDefinition: StatusDefinition.getLiteral("processing")}),
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean"
      }),
      new LiteralDefinition({
        literalName: "statusDone",
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("status")})
          .property({propertyDefinition: StatusDefinition.getLiteral("done")}),
        rdfDataType: "http://www.w3.org/2001/XMLSchema#boolean"
      }),
      new LiteralDefinition({
        literalName: "statusMessage",
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("status")})
          .property({propertyDefinition: StatusDefinition.getLiteral("message")})
      }),
      new LiteralDefinition({
        literalName: "statusError",
        linkPath: new LinkPath()
          .step({linkDefinition: this.getLink("status")})
          .property({propertyDefinition: StatusDefinition.getLiteral("error")})
      }),
    ];
  }
}




