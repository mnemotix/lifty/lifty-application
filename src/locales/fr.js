export const fr = {
  "BREADCRUMB": {
    "PERSONS": "Agents"
  },
  "ACTIONS": {
    "ADD": "Ajouter",
    "CANCEL": "Annuler",
    "CLOSE": "Fermer",
    "CHANGE_IMAGE": "Modifier l'image",
    "DELETE": "Effacer",
    "GO_BACK": "Retour",
    "UPLOAD_CSV": "  CSV",
    "ONE_VALUE_ONLY": "Une seule valeur possible",
    "NO_RESULT": "Aucun résultat ne correspond à votre recherche",
    "TYPE_SOMETHING": "Taper quelque chose pour commencer une recherche....",
    "PROCEED": "Valider",
    "REMOVE_IMAGE": "Supprimer",
    "SAVE": "Enregistrer",
    "SUCCESS": "Enregistré avec succès",
    "UPDATE": "Modifier",
    "LOADING": "Chargement...",
    "NEXT": "Suivant",
    "PREVIOUS": "Précédent",
    "NOT_ALLOWED": "Vous n'avez pas accès à cette fonctionnalité."
  },
  "APOLLO": {
    "ERROR": {
      "MALFORMED_REQUEST": "Une erreur s'est produite.",
      "SERVER_ERROR": "Une erreur s'est produite.",
      "USER_NOT_EXISTS_IN_GRAPH": "Votre compte est désynchronisé, contactez un administrateur."
    }
  },
  "APP_BAR": {
    "BUTTONS": {
      "BACK": "Retour"
    },
    "MENU": {
      "MY_PROFILE": "Mon profil",
      "LOGIN": "Se connecter",
      "LOGOUT": "Se déconnecter",
      "CONCEPT": "Concept",
      "REFERENTIALS": "Référentiels",
      "DEMO": "Démonstration",
      "AGENTS": "Agents",
      "JOBS": "Jobs",
      "CONTACT": "Contact"
    }
  },
  "COMMON": {
    "CREATED_AT": "Créé le",
    "CREATED_BY": "Créé par",
    "END_AT": "Fin",
    "START_AT": "Début",
    "TITLE": "Titre",
    "UPDATED_AT": "Modifié le",
    "IS_ADMIN": "Admin",
    "LINK": "Lien"
  },
  "SIGN_IN": {
    "EMAIL": "Adresse mail",
    "PASSWORD": "Mot de passe",
    "PASSWORD_FORGOTTEN": "Mot de passe oublié",
    "REDIRECT_SIGN_UP": "Créer un compte",
    "REMEMBER_ME": "Se souvenir de moi.",
    "SUBMIT": "Se connecter",
    "TITLE": "Se connecter"
  },
  "SIGN_UP": {
    "EMAIL": "Adresse mail",
    "FIRST_NAME": "Prénom",
    "LAST_NAME": "Nom",
    "PASSWORD": "Mot de passe",
    "REDIRECT_SIGN_IN": "Vous avez déjà un compte ? Se connecter.",
    "SUBMIT": "Créer le compte",
    "TITLE": "Créer un compte",
    "VALIDATE_PASSWORD": "Confirmation du mot de passe"
  },
  "PERSON": {
    "CREATE": "Nouvelle personne",
    "CREATE_TITLE": "Créer une nouvelle personne",
    "DELETE": {
      "DELETE_DEFINITELY": "SUPPRIMER DEFINITIVEMENT LA PERSONNE",
      "DONE_CONFIRMATION": "Personne supprimée avec succès",
      "I_UNDERSTAND": "Supprimer"
    },
    "ISADMIN": "Admin",
    "FIRST_NAME": "Prénom",
    "LAST_NAME": "Nom",
    "NEW": "Ajouter une personne",
    "COMMENT": "Commentaire",
    "EMAIL": "Email",
    "LIST": "Liste des agents",
    "CANUSEPREMIUMVOC": "Peut utiliser les référentiels PREMIUM pour le prochain job crée"
  },
  "JOB": {
    "ADD_YOUR_CSV": "Ajouter votre fichier CSV",
    "ANALYSE_FILE": "Analyser le fichier",
    "CSV_UPLOADED": "Fichier CSV envoyé avec succès",
    "ON_SCHEME": "sur le schema",
    "JOBCOUNT": "Nombre de Jobs",
    "LIST": "Liste des jobs",
    "MY_LIST":"Liste de mes jobs",
    "URI": "Uri",
    "CREATE": "Créer un job pour cet agent",
    "CREATE_PREMIUM": "Créer un job PREMIUM pour cet agent",
    "CANUSEPREMIUMVOC": "Peut utiliser les référentiels PREMIUM",
    "ERROR_HAPPENED": "Une erreur est survenue",
    "CREATING": "Création du job...",
    "PUBLIC_URL": "Url public",
    "REFERENTIAL": "Référentiel",
    "ID": "ID",
    "INDEXNAME": "Nom de l'index",
    "ALL": "Tous",
    "PENDING": "En attente",
    "PROCESSING": "En cours",
    "DONE": "Fini",
    "ERROR": "Erreur",
    "UNUSED": "CSV non ajouté",
    "MESSAGE": "Message",
    "STATUS": "Status",
    "NOSTATUS": "ERREUR, Pas de status",
    "OWNER": "Propriétaire",
    "START": "Démarrer la tâche"
  },
  "PROJECT": {
    "YOUR_PROJECT": "Votre projet"
  },
  "REFERENTIAL": {
    "PREMIUM": "Premium",
    "PREMIUM_ONLY": "Réservé à la version Premium",
    "LIST": "Liste des référentiels",
    "PREFLABEL": "Description",
    "ALTLABEL": "Référentiel",
    "SCHEMES": "Schemes",
    "CHOOSE_ONE_SCHEME": "Choisir un schéma"
  },
  "FORM_ERRORS": {
    "FIELD_ERRORS": {
      "EMAIL_ALREADY_REGISTERED": "Cette adresse de courriel est déjà utilisée",
      "INVALID_EMAIL": "Format de l'adresse de courriel invalide",
      "PASSWORD_TOO_SHORT": "Mot de passe trop court",
      "PASSWORDS_DO_NOT_MATCH": "La confirmation ne correspond pas au nouveau mot de passe",
      "REQUIRED": "Champ obligatoire",
      "REQUIRED_S": "Champ(s) obligatoire(s)",
      "TOO_SHORTS": "Champ(s) trop court(s)",
      "WRONG_OLD_PASSWORD": "L'ancien mot de passe est incorrect"
    },
    "GENERAL_ERRORS": {
      "DISABLED_BECAUSE_NO_MODIFICATION": "Aucune modification n'a été faite",
      "FORM_VALIDATION_ERROR": "Certains champs sont invalides",
      "INVALID_CREDENTIALS": "Ces identifiants ne sont pas valides",
      "UNEXPECTED_ERROR": "Un problème non identifié nous empêche d'effectuer cette action. Réessayez plus tard",
      "USER_MUST_BE_AUTHENTICATED": "Vous devez être connecté avec un compte utilisateur pour effectuer cette action",
      "USER_NOT_ALLOWED": "Vous n'avez pas la persmission d'effectuer cette action"
    }
  },
  "RESET_PASSWORD": {
    "CONFIRM": "Envoyer le mail",
    "CONFIRMATION": "Email envoyé, verifiez également votre dossier SPAM",
    "CHECK_SPAM": "Verifier également votre dossier SPAM",
    "SEND_RESET_TO_EMAIL": "Reinitialiser le mot de passe via le mail"
  },
  "REMOTE_TABLE": {
    "ACTIONS": {
      "FILTERS": "Filtres",
      "REMOVE": "Supprimer",
      "REMOVE_CONFIRM_TEXT": "Êtes-vous sûr de vouloir supprimer cet objet ?",
      "REMOVE_CONFIRM_TEXT_plural": "Êtes-vous sûr de vouloir supprimer ces {{count}} objets ?",
      "REMOVE_plural": "Supprimer ({{count}})",
      "REMOVE_SUCCESS": "Élements supprimés avec succès"
    },
    "BODY": {
      "NO_MATCH": "Aucun résultat trouvé...",
      "TOOLTIP": "Sort"
    },
    "FILTER": {
      "ALL": "Tout",
      "RESET": "Réinitialiser",
      "TITLE": "Filtres",
      "NOFILTER": "Aucun"
    },
    "PAGINATION": {
      "DISPLAY_ROWS": "de",
      "NEXT": "Page suivante",
      "PREVIOUS": "Page précédente",
      "ROWS_PER_PAGE": "Lignes par page:"
    },
    "SELECTED_ROWS": {
      "DELETE": "Supprimer",
      "DELETE_ARIA": "Supprimer les colonnes sélectionnées",
      "TEXT": "colonne(s) sélectionnée(s)"
    },
    "TOOLBAR": {
      "DOWNLOAD_CSV": "Télécharger CSV",
      "FILTER_TABLE": "Filtrer",
      "PRINT": "Imprimer",
      "SEARCH": "Rechercher",
      "VIEW_COLUMNS": "Colonnes visibles"
    },
    "VIEW_COLUMNS": {
      "TITLE": "Colonnes visibles",
      "TITLE_ARIA": "Afficher/Cacher les colonnes du tableau"
    }
  }
};
