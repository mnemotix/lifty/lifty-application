import dotenv from 'dotenv';

export function setupEnvironmentVar() {
  dotenv.config();
}
