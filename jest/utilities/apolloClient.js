import {ApolloClient, HttpLink} from 'apollo-boost';
import {InMemoryCache, IntrospectionFragmentMatcher} from 'apollo-cache-inmemory';
import {FragmentTypes} from '../../src/generated/FragmentTypes';

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData: FragmentTypes
});

export const CacheWithFragmentMatcher = new InMemoryCache({fragmentMatcher});


export const apolloClient = new ApolloClient({
  link: new HttpLink({
    uri: `${process.env.APP_URL}/graphql`,
    fetch: (uri, options) => {
      options.credentials = "omit";
      options.referrer = process.env.APP_URL;
      return global.originalFetch(uri, options);
    }
  }),
  cache: CacheWithFragmentMatcher
});
