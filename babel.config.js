const regeneratorRuntime = require('regenerator-runtime');

module.exports = function (api) {
  api.cache(true);

  const presets = [];
  const plugins = [ 
    ['@babel/proposal-object-rest-spread'],
    ['@babel/proposal-optional-chaining'],
    [
      'babel-plugin-import',
      {
        libraryName: '@material-ui/core',
        // Use "'libraryDirectory': ''," if your bundler does not support ES modules
        libraryDirectory: 'esm',
        camel2DashComponentName: false,
      },
      'core',
    ],
    [
      'babel-plugin-import',
      {
        libraryName: '@material-ui/icons',
        // Use "'libraryDirectory': ''," if your bundler does not support ES modules
        libraryDirectory: 'esm',
        camel2DashComponentName: false,
      },
      'icons',
    ]
  ];

  return {
    presets,
    plugins,
    sourceMaps: 'inline',
    ignore: [process.env.NODE_ENV !== 'test' ? '**/*.test.js' : null].filter((n) => n),
  };
};
